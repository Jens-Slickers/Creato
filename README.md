# Creato
Minimal Crossplatform Graphics and Input API/Engine.
This API is currently not fully functional, and using it is probably quite a pain.
This was originally created as a very minimal base for some small projects of mine.

The Engine supports multiple windows, and multiple scene trees, which can be added to any window at any time.
The UI elements include only a select few (Sprites, Text, Button)

Licenses are in the LICENSE file.

## Dependencies:

### Not contained in this project:

1. Vulkan SDK (Link needed)
2. GLFW (Link needed)
3. GLM (Link needed)
4. BOOST (Link needed)

### Contained in this project (in the lib folder)

1. stb_image.h (Link needed)
2. stb_image_write.h (Link needed)
3. stb_truetype.h (Link needed)
4. tiny_obj_loader.h (Link needed)
5. tiny_gltf.h (Link needed) (the source repo also contains the json.hpp below)
6. json.hpp (Link needed)

## Setup

### Environment Setup
#### Windows:
I used VSCode with mysys/mingw as compiler. Some VSCode extenisons I used:
 - C/C++ Intellisense (https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools),
 - Shader language support (https://marketplace.visualstudio.com/items?itemName=slevesque.shader)

You can find a guide to use install and use MinGW with vscode here: https://code.visualstudio.com/docs/cpp/config-mingw.
I needed several tries and faced countless breakdowns, so don't give up.
Also remember to restart your PC when editing path variables.

#### Linux:
???

#### Mac:
???

### Vulkan (https://vulkan.lunarg.com/)
You need to install the Vulkan SDK, typically you should choose the newest available stable release for your platform.
Just follow the installation menu, it should be fairly straight forward. You might need to restart your PC.

### GLFW (https://www.glfw.org/download.html)
You can download the binaries or build it from scratch. I simply used the binaries, as they are quite easy to setup. Remember to download the same bit version as your compiler (32 vs 64 bit)

### GLM (https://github.com/g-truc/glm)
GLM is a linear algebra library. GLM is increadibly easy to use, compared to other alternatives, and it is designed to be used with Graphics APIs. However, it might not be the fastest linear algebra liberay in all cases, and you can always add a second, faster, library so you can use glm for rendering stuff, and others for everything else.

### BOOST (Link needed)
You might need the the boost library at some point, and build the parts of it that are not prebuild. (tutorial link needed).

## Credits
When learning the Vulkan API, https://vulkan-tutorial.com/ was severly helpful. Depending on your prefered language, there might be some youtube tutorials available.
