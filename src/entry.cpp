/** Entry point for Creato.
 * 
 */

#include <application.hpp>
#include <iostream>

int main() {

    try
    {
        creato::application_init();

        creato::application_loop();

        creato::application_shutdown();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    


}