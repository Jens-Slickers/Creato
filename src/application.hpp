/**
 * 
 */

#pragma once

namespace creato
{

    /* ========== GLOBALS ========== */

    /* ========== FUNCTIONS ========== */

    void application_init();

    void application_loop();

    void application_shutdown();
    
} // namespace creato
