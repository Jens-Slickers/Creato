/**
 * 
 */

//Attached Header
#include <application.hpp>

//BOOST
#include <boost/chrono.hpp>

//STD
#include <vector>
#include <stdint.h>
#include <iostream>

//In-House
#include <nodes/2d/label.hpp>
#include <nodes/2d/panel.hpp>
#include <utils/filesystem.hpp>
#include <utils/graphics.hpp>
#include <utils/input.hpp>

namespace creato
{

    /* ========== GLOBALS ========== */

    ::boost::chrono::time_point prevTime = ::boost::chrono::high_resolution_clock::now();
    ::std::vector<graphics::Window*> windows = ::std::vector<graphics::Window*>(0);
    graphics::Model* p_Model;
    graphics::Model* p_UIModel;

    /* ========== FUNCTIONS ========== */

    void application_init() {

        graphics::init();
        input::init();

        //TODO preload fonts
        
        //p_Model = graphics::createModel(filesystem::makeAbsoluteProcPath("/models/viking_room/viking_room.obj"), filesystem::makeAbsoluteProcPath("/models/viking_room/viking_room.png"), startWindow);

        graphics::Node* label = graphics::createLabel("Hallo", "OpenSans/OpenSans-Regular.ttf", 64, {0, 0}, 1);
        graphics::Panel* panel = new graphics::Panel({0, 0}, {200.0f, 200.0f}, 0, {100, 100, 100, 255}, 2.0f, {200, 200, 200, 255}); //TODO make Label have an optional Panel?
        label->attachChild(panel);

        graphics::Window* startWindow = graphics::createWindow("Creato", label);
        input::initWindowInput(startWindow->glfwWindow);

        windows.push_back(startWindow);

        // API-info: Create as many windows as you like.
        //graphics::Window* secondaryTestWindow = new graphics::ClassicWindow("Creato Window 2");
        //windows.push_back(secondaryTestWindow);

    }

    void application_loop() {
        float cTime = 0;
        uint32_t frames = 0;
        while (windows.size() > 0) {

            //static ::boost::chrono::time_point startTime = ::boost::chrono::high_resolution_clock::now();

            //time in seconds
            //float timeSinceStart = ::boost::chrono::duration_cast<::boost::chrono::microseconds>(::boost::chrono::high_resolution_clock::now() - startTime).count() / 1000.0f;
            float deltaTime = ::boost::chrono::duration_cast<::boost::chrono::microseconds>(::boost::chrono::high_resolution_clock::now() - prevTime).count() / 1000.0f;
            prevTime = ::boost::chrono::high_resolution_clock::now();

            cTime += deltaTime;
            ++frames;

            if (cTime > 1000) {
                //interface::createLabel(windows[0], {std::rand() % windows[0]->windowSize.width, std::rand() % windows[0]->windowSize.height, 0}, "Hallo", "OpenSans/OpenSans-Regular.ttf", 64);
                std::cout << frames << " frames in " << cTime << " milliseconds." << std::endl;
                cTime = 0;
                frames = 0;
            }

            glfwPollEvents(); //TODO InputModule
            //p_Model->rotation = glm::rotate(p_Model->rotation, 0.01f * deltaTime * glm::radians(1.0f), glm::vec3(0.0f, 0.0f, 1.0f));

            for (uint32_t i = 0; i < windows.size(); i++) {
                if(!glfwWindowShouldClose(windows[i]->glfwWindow)){
                    drawframe(windows[i]);
                } else {
                    delete windows[i];
                    windows.erase(windows.begin() + i);
                    break;
                }
            }
        }
    }

    void application_shutdown() {
        //close all Windows

        for (uint32_t i = 0; i < windows.size(); i++) {
            graphics::destroyWindow(windows[i]);
        }
        windows.clear();

        graphics::shutdown();
        input::shutdown();

        __debugbreak();
    }
    
} // namespace creato
