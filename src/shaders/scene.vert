#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout(binding = 0) uniform uniformBufferobject{
    mat4 model;
    mat4 viewProjection;
}ubo;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 inTexCoord;

layout(location = 0) out vec2 fragTexCoord;

void main() {
    gl_Position = ubo.viewProjection * ubo.model * vec4(position, 1.0);
    fragTexCoord = inTexCoord;
}