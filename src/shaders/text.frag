#version 450
#extension GL_ARB_separate_shader_objects : enable

//Inputs from Vertexshader

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;

//Shader Functions
void main() {
    vec4 color = texture(texSampler, fragTexCoord);
    if (color.a < 0.5f) {
        discard;
    } else {
        outColor = vec4(color.r, color.g, color.b, 1);
    }
}