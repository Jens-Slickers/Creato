/**
 * 
 */
               
//Attached Header
#include <nodes/2d/label.hpp>

//In-House
#include <utils/defines.hpp>
#include <utils/filesystem.hpp>

namespace creato::graphics {

    Label::~Label() {
        if (p_Model) {
            destroyModel(p_Model);
        }
    }

    Label* createLabel(std::string text, std::string font, uint32_t fontSize, glm::vec2 position, int posZ, glm::vec2 size, float characterMargin, float lineMargin, float vAlign, float hAlign, RGBA8 fontColor) {
        return new Label(text, font, fontSize, position, posZ, size, characterMargin, lineMargin, vAlign, hAlign, fontColor);
    }

    void destroyLabel(Label* p_Label) {
        delete p_Label;
    }

    void Label::setPos(glm::vec3 position) {
        p_Model->position = position;
        graphics::updateModel(p_Model);
    }

    void Label::setSize(glm::ivec2 size) {
        this->size = size;
    }

    void Label::setText(std::string text) {
        this->text = text;

        Window* p_Window = p_Model->p_Window;
        onDeactivate();
        onActivate(p_Window);
    }

    void Label::addText(std::string text) {
        this->text += text; //TODO this is recursive if operator is overloaded
    }

    void Label::removeLastCharacter() {
        //TODO remove last 6 indices, and last 4 vertices
        graphics::updateModel(p_Model);
    }

    void Label::clearText() {
        text = std::string(""); //TODO relaod label
    }

    void Label::setFont(std::string font) {
        this->font = font;
    }

    void Label::setFontSize(uint32_t fontSize) {
        this->fontSize = fontSize;
    }

    void Label::setAlignment(float hAlign, float vAlign) {
        this->hAlign = hAlign;
        this->vAlign = vAlign;
        setText(text);
    }

    void Label::onActivate(Window* p_Window) {
        Mesh* p_Mesh = new Mesh();
        Texture* p_Texture = nullptr;

        glm::vec2 cursor = {0, 0};

        for (uint32_t i = 0; i < text.size(); i++) {
            auto charVertexList = getCharacterVertices(font, text.at(i), cursor, &p_Texture);
            for (auto& v : charVertexList) {
                p_Mesh->v_VertexList.push_back(v);
            }
            p_Mesh->v_IndexList.push_back(i*4 + 2); //TODO make getCharVerts take a mesh and add them automatically
            p_Mesh->v_IndexList.push_back(i*4 + 1);
            p_Mesh->v_IndexList.push_back(i*4 + 0);

            p_Mesh->v_IndexList.push_back(i*4 + 0);
            p_Mesh->v_IndexList.push_back(i*4 + 3);
            p_Mesh->v_IndexList.push_back(i*4 + 2);
        }

        createVertexBuffer(p_Mesh->vertexBufferCase, p_Mesh->v_VertexList);
        createIndexBuffer(p_Mesh->indexBufferCase, p_Mesh->v_IndexList);

        p_Model = createInterfaceModel(p_Mesh, p_Texture, p_Window);
        p_Model->position.x = position.x;
        p_Model->position.y = position.y;
        p_Model->position.z = posZ;

        //updateModel(); unnessesary since the actual update is defered until the next drawFrame() call, and this was called implicitly in createInterfaceModel.

        if(visible) {
            p_Model->p_Window->transparentInterfaceRenderList.insert(p_Model);
        }

    };
    
    void Label::onDeactivate() {
        if (p_Model) {
            destroyModel(p_Model);
        }
    };

    void Label::onShow() {
        if (p_Model) {
            p_Model->p_Window->transparentInterfaceRenderList.insert(p_Model);
            p_Model->p_Window->b_ReRecordCommandBuffers = true;
        }
    };

    void Label::onHide() {
        if (p_Model) {
            p_Model->p_Window->transparentInterfaceRenderList.erase(p_Model);
            p_Model->p_Window->b_ReRecordCommandBuffers = true;
        }
    };

} // namespace creato::graphics
