/**
 * 
 */

#pragma once

//In-House
#include <nodes/2d/node2d.hpp>

//TODO pretty / fancy label with animation, font bg image (image in fontshape)
//TODO Label that displays a fontsize at another size

namespace creato::graphics {

    //TODO not pixelperfect Labelclass:
    class Label : public Node2D {
        public:

            void setPos(glm::vec3 position);
            void setSize(glm::ivec2 size);
            void setText(std::string text);
            void addText(std::string text);
            void removeLastCharacter();
            void clearText();
            void setFont(std::string font);          //this includes changing from italic to regular to bold etc.
            void setFontSize(uint32_t fontSize);
            void setAlignment(float hAlign, float vAlign);

            Label(std::string text, std::string font, uint32_t fontSize, glm::vec2 position, int posZ, glm::vec2 size, float characterMargin, float lineMargin, float vAlign, float hAlign, RGBA8 fontColor) : Node2D(position, size, posZ), text(text), font(font), fontSize(fontSize), characterMargin(characterMargin), lineMargin(lineMargin), hAlign(hAlign), vAlign(vAlign), fontColor(fontColor) {};

            virtual ~Label();
            
            virtual void onActivate(Window* p_Window) override;
            virtual void onDeactivate() override;
            virtual void onShow() override;
            virtual void onHide() override;
            //virtual void onAttachChild(Node* attachedChild) override;
            //virtual void onDetachChild(Node* detachedChild) override;
            //virtual void onAttachedToParent() override;
            //virtual void onDetachedFromParent(Node* oldParent) override;

        protected:

            Model* p_Model;

            std::string text = std::string("");
            std::string font = std::string("");
            uint32_t fontSize = 0;
            
            float characterMargin = 0;
            float lineMargin = 0;

            //0 to 1
            float hAlign = 0;
            float vAlign = 0;

            //R8G8B8A8 Format
            RGBA8 fontColor = {255, 255, 255, 255};
    };

    Label*  createLabel(std::string text, //TODO reorder
                        std::string font,
                        uint32_t fontSize,
                        glm::vec2 position,
                        int posZ = 0,
                        glm::vec2 size = {0, 0},
                        float characterMargin = 1,
                        float lineMargin = 1,
                        float vAlign = 0,
                        float hAlign = 0,
                        RGBA8 fontColor = {255, 255, 255, 255} //TODO parent optional param for auto addChild()
    );

} // namespace creato::graphics
