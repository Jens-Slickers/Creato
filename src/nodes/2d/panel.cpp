/**
 * 
 */

#include <nodes/2d/panel.hpp>

namespace creato::graphics {

    void Panel::onActivate(Window* p_Window) {
        
        if (borderSize > 0.0f) {
            Mesh* p_Mesh = new Mesh();
            p_Mesh->v_VertexList = {
                Vertex({0.0f,   0.0f,   0.0f}, {0.0f, 0.0f}),
                Vertex({0.0f,   size.y, 0.0f}, {0.0f, 1.0f}),
                Vertex({size.x, 0.0f,   0.0f}, {0.5f, 0.0f}),
                Vertex({size.x, size.y, 0.0f}, {0.5f, 1.0f}),

                Vertex({borderSize,          borderSize,          0.1f}, {0.5f, 0.0f}),
                Vertex({borderSize,          size.y - borderSize, 0.1f}, {0.5f, 1.0f}),
                Vertex({size.x - borderSize, borderSize,          0.1f}, {1.0f, 0.0f}),
                Vertex({size.x - borderSize, size.y - borderSize, 0.1f}, {1.0f, 1.0f})
            };
            p_Mesh->v_IndexList = {0, 1, 2, 3, 2, 1,   4, 5, 6, 7, 6, 5};

            createVertexBuffer(p_Mesh->vertexBufferCase, p_Mesh->v_VertexList);
            createIndexBuffer(p_Mesh->indexBufferCase, p_Mesh->v_IndexList);
            
            RGBA8_M color = {borderColor[0], borderColor[1], borderColor[2], borderColor[3], baseColor[0], baseColor[1], baseColor[2], baseColor[3]};
            Texture* p_Texture = createMultiColorTexture(color, false);
            p_Model = createInterfaceModel(p_Mesh, p_Texture, p_Window);
            p_Model->position.x = position.x;
            p_Model->position.y = position.y;
            p_Model->position.z = posZ;

            //updateModel(); unnessesary since the actual update is defered until the next drawFrame() call, and this was called implicitly in createInterfaceModel.

            if(visible) {
                p_Model->p_Window->transparentInterfaceRenderList.insert(p_Model);
            }
        } else {
            Mesh* p_Mesh = new Mesh();
            p_Mesh->v_VertexList = {
                Vertex({0.0f,   0.0f,   0.0f}, {0.0f, 0.0f}),
                Vertex({0.0f,   size.y, 0.0f}, {0.0f, 1.0f}),
                Vertex({size.x, 0.0f,   0.0f}, {1.0f, 0.0f}),
                Vertex({size.x, size.y, 0.0f}, {1.0f, 1.0f})
            };
            p_Mesh->v_IndexList = {0, 1, 2, 3, 2, 1};

            createVertexBuffer(p_Mesh->vertexBufferCase, p_Mesh->v_VertexList);
            createIndexBuffer(p_Mesh->indexBufferCase, p_Mesh->v_IndexList);
            Texture* p_Texture = createSingleColorTexture(baseColor, false);
            p_Model = createInterfaceModel(p_Mesh, p_Texture, p_Window);
            p_Model->scale = glm::vec3(1, 1, 1); //TODO make this into node2d member?
            p_Model->position.x = position.x;
            p_Model->position.y = position.y;
            p_Model->position.z = posZ;

            //updateModel(); unnessesary since the actual update is defered until the next drawFrame() call, and this was called implicitly in createInterfaceModel.

            if(visible) {
                p_Model->p_Window->transparentInterfaceRenderList.insert(p_Model);
            }
        }
    };

    void Panel::onDeactivate() {
        if (p_Model) {
            destroyModel(p_Model);
        }
    };

    void Panel::onShow() {};
    void Panel::onHide() {};
    
} // namespace creato::graphics
