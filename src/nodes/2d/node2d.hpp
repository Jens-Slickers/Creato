/**
 * 
 */

#pragma once

//In-House
#include <utils/graphics.hpp>

namespace creato::graphics {

    class Node2D : public Node {
        public:
            glm::vec2 position = {0, 0};
            glm::vec2 size = {0, 0};

            // Can be between 0 and 1, indicates what percentage of space is to the left (or top) of this element.
            // E.g. When position is set to {0, 0} and alignment to {0.5f, 0.5f}, the element will be centered in the parent-element's space.
            glm::vec2 alignment = {0.0f, 0.0f};

            int posZ = 1; //TODO -1? limits?

            //0 to 1
            float vAlign = 0;
            float hAlign = 0;

            Node2D(glm::vec2 position, glm::vec2 size, int posZ) : position(position), size(size), posZ(posZ) {};

            virtual ~Node2D() {};

            //virtual void onActivate(Window* p_Window) {};
            //virtual void onDeactivate() {};
            //virtual void onShow() {};
            //virtual void onHide() {};
            //virtual void onAttachChild(Node* attachedChild) {};
            //virtual void onDetachChild(Node* detachedChild) {};
            //virtual void onAttachedToParent() {};
            //virtual void onDetachedFromParent(Node* oldParent) {};
    };

} // namespace creato::graphics