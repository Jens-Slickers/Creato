/**
 * 
 */

#pragma once

#include <nodes/2d/node2d.hpp>

namespace creato::graphics {

    class Panel : public Node2D {
        public:

            RGBA8 baseColor = {255, 255, 255, 255};
            float borderSize = 0.0f;
            RGBA8 borderColor = {0, 0, 0, 0};

            Panel(glm::vec2 position, glm::vec2 size, int posZ, RGBA8 baseColor, float borderSize = 0.0f, RGBA8 borderColor = {0, 0, 0, 0}) : Node2D(position, size, posZ), baseColor(baseColor), borderSize(borderSize), borderColor(borderColor) {};
            ~Panel() {};

            virtual void onActivate(Window* p_Window) override;
            virtual void onDeactivate()override;
            virtual void onShow() override;
            virtual void onHide() override;
            //virtual void onAttachChild(Node* attachedChild) override;
            //virtual void onDetachChild(Node* detachedChild) override;
            //virtual void onAttachedToParent() override;
            //virtual void onDetachedFromParent(Node* oldParent) override;

        protected:
            Model* p_Model;

    };
    
} // namespace creato::graphics
