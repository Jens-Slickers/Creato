/**
 * 
 */

//Attached Header
#include <utils/graphics.hpp>

//STD
#include <cstring>
#include <string>
#include <fstream>

//BOOST
#include <boost/chrono.hpp>

//GLM
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

//STB
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>

//TOL
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

//In-House
#include <utils/defines.hpp>
#include <utils/filesystem.hpp>

/* ========== THROW ========== */

#define ASSERT_VKRESULT_THROW(text, vkResult)   if (vkResult) {\
                                                    char* message = nullptr;\
                                                    sprintf(message, "%s %s %d", text, "VkResult was:", vkResult);\
                                                    throw std::runtime_error(message);\
                                                }

/* ========== LGRI AS PARAM ========== */

#define ASSERT_VKRESULT_LGRI_LGR_T(vkRes, lgResInfo, lgRes, text)   if (vkResult != VK_SUCCESS && lgResInfo) {\
                                                                        __debugbreak();\
                                                                        lgResInfo->lgResult = lgRes;\
                                                                        lgResInfo->vkResult = vkRes;\
                                                                        lgResInfo->message = text;\
                                                                    }

#define LGRI_SUCCESS nullptr;

/* ========== LGRI AS RETURN ========== */

#define ASSERT_VKRESULT_LGR_T(vkRes, lgRes, text)   if (vkRes != VK_SUCCESS) {\
                                                        __debugbreak();\
                                                        CreatoGraphicsResultInfo* res = new CreatoGraphicsResultInfo();\
                                                        res->lgResult = lgRes;\
                                                        res->vkResult = vkRes;\
                                                        res->message = text;\
                                                        return res;\
                                                    }

#define ASSERT_VKRESULT_LGR(vkRes, lgRes) ASSERT_VKRESULT_LGR_T(vkRes, lgRes, "")

//#define CREATO_UTILS_GRAPHICS_DEBUG_BUILD

#ifdef CREATO_GLOBAL_DEBUG_BUILD
#define CREATO_UTILS_GRAPHICS_DEBUG_BUILD
#endif

#ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
#include <iostream>
#endif

namespace creato::graphics {
    
    typedef struct Application {

        VkInstance instance;

        VkPhysicalDevice physicalDevice;

        VkDevice device;

        VkQueue queue;
        uint32_t queueFamilyIndex;

        const VkFormat colorFormat = VK_FORMAT_B8G8R8A8_UNORM; //TODO test if this is available?

        VkRenderPass renderPass;

        VkDescriptorSetLayout descriptorSetLayout;

        VkPipelineLayout pipelineLayout;
        VkPipeline scenePipeline;
        VkPipeline textPipeline;

        VkCommandPool commandPool;

        const uint32_t maxInFlightFrames = 3;

        VkSampleCountFlagBits msaaSampleCount = VK_SAMPLE_COUNT_1_BIT;

    } Application;

    Application g_Application;

    std::unordered_set<Model*> updateModelList;

    // This is needed for callbacks (like resize), as they receive the glfw window, and need to be forwarded to their attached vulkan window.
    // All Vulkan windows need to be inserted here upon creation and removed once deleted.
    std::unordered_map<GLFWwindow*, Window*> windowMap;

    typedef struct CharacterBitmap {
        uint32_t texPos;              //Position in the texture.
        uint32_t refCounter = 0;
        glm::ivec2 pos;
        glm::ivec2 size;
        RGBA8_M bitmap;

        CharacterBitmap(uint32_t texPos, glm::vec2 pos, glm::vec2 size, RGBA8_M bitmap) : texPos(texPos), pos(pos), size(size), bitmap(bitmap) {};

    } CharacterBitmap;

    typedef struct FontBitmap {
        Texture* p_Texture;
        RGBA8_M pixels;
        std::unordered_set<CharacterBitmap*> characters;
    } FontBitmap;                                                                            

    // All Character-Bitmaps, sortet by filepath, size, and codepoint. the bitmap is not in RGBA!, it is just a bitmap with values from 0 to 256.
    // To create a unsigned char* in RGBA Format (for Vulkan) e.g. you can take any RGB color you want, and add the bitmap value as alpha value.
    std::unordered_map<std::string, std::unordered_map<uint32_t, std::unordered_map<unsigned long, CharacterBitmap*>>> characterBitMaps;
    std::unordered_map<std::string, std::unordered_map<uint32_t, std::unordered_map<unsigned long, Texture*>>> characterTextures;

    std::unordered_map<std::string, std::unordered_map<unsigned long, CharacterBitmap*>> characterSDFs;
    std::unordered_map<std::string, FontBitmap*> fontSDFTextures;


    /* ===== ===== Forward Declarations ===== ===== */

    static void recreateWindow(Window* window);

    /* ========== Functions ========== */

#ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD // debug print functions

    static void debugPrintInstanceLayerProperties(::std::vector<VkLayerProperties>& instanceLayers) {
        ::std::cout << "No. of instance layers:" << instanceLayers.size() << ::std::endl;
        ::std::cout << ::std::endl;
        for (VkLayerProperties instanceLayer : instanceLayers) {
            ::std::cout << "   - NAME: " << instanceLayer.layerName << ::std::endl;
            ::std::cout << "      - IMPL: " << instanceLayer.implementationVersion << ::std::endl;
            ::std::cout << "      - SPEC: " << instanceLayer.specVersion << ::std::endl;
            ::std::cout << "      - DESC: " << instanceLayer.description << ::std::endl;
            ::std::cout << ::std::endl;
        }
    }

    static void debugPrintPhysicalDeviceProperties(const ::std::vector<VkPhysicalDevice>& physicalDevices, VkSurfaceKHR surface) {

        ::std::cout << "Physical Devices:" << physicalDevices.size() << ::std::endl;
        ::std::cout << ::std::endl;

        for (VkPhysicalDevice physicalDevice : physicalDevices)
        {
            
            VkPhysicalDeviceProperties physicalDeviceProperties;
            vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);

            uint32_t apiVersion = physicalDeviceProperties.apiVersion;
            ::std::cout << "  - " << physicalDeviceProperties.deviceName << ":" << ::std::endl;
            ::std::cout << ::std::endl;
            ::std::cout << "     - API Version:      " << VK_VERSION_MAJOR(apiVersion) << "." << VK_VERSION_MINOR(apiVersion) << "." << VK_VERSION_PATCH(apiVersion) << ::std::endl;
            ::std::cout << "     - Driver Version:   " << physicalDeviceProperties.driverVersion << ::std::endl;
            ::std::cout << "     - Vendor ID:        " << physicalDeviceProperties.vendorID << ::std::endl;
            ::std::cout << "     - Device ID:        " << physicalDeviceProperties.deviceID << ::std::endl;
            ::std::cout << "     - Device Type:      " << physicalDeviceProperties.deviceType << ::std::endl;
            ::std::cout << "     - Discrete Q Prios  " << physicalDeviceProperties.limits.discreteQueuePriorities << ::std::endl;
            //std::cout << "     - PipeCache UUID:   " << ::std::endl;   //TODO Research, important for shaders
            ::std::cout << ::std::endl;

            VkPhysicalDeviceFeatures physicalDeviceFeatures;                                                //TODO activate some features
            vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
            ::std::cout << "     - Features:         " << ::std::endl;
            ::std::cout << "        - Geometry Shader:  " << physicalDeviceFeatures.geometryShader << ::std::endl;        //TODO just an example, check for needed features!
            ::std::cout << ::std::endl;

            VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;                                //TODO research pls
            vkGetPhysicalDeviceMemoryProperties(physicalDevice, &physicalDeviceMemoryProperties);

            uint32_t queueFamilyCount = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
            ::std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProperties.data());

            queueFamilyCount = 0;
            for(VkQueueFamilyProperties queueFamilyProperty : queueFamilyProperties) {
                ::std::cout << "     - Queue Fmaily:                     " << queueFamilyCount++ << ::std::endl;
                ::std::cout << "        - VK_QUEUE_GRAPHICS_BIT:            " << (queueFamilyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT) << ::std::endl;
                ::std::cout << "        - VK_QUEUE_COMPUTE_BIT:             " << (queueFamilyProperty.queueFlags & VK_QUEUE_COMPUTE_BIT) << ::std::endl;
                ::std::cout << "        - VK_QUEUE_TRANSFER_BIT:            " << (queueFamilyProperty.queueFlags & VK_QUEUE_TRANSFER_BIT) << ::std::endl;
                ::std::cout << "        - VK_QUEUE_SPARSE_BINDING_BIT:      " << (queueFamilyProperty.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) << ::std::endl;
                ::std::cout << "        - Queue Count:                      " << queueFamilyProperty.queueCount << ::std::endl;
                ::std::cout << "        - Timestamp Valid Bits:             " << queueFamilyProperty.timestampValidBits << ::std::endl;
                uint32_t width = queueFamilyProperty.minImageTransferGranularity.width;
                uint32_t height = queueFamilyProperty.minImageTransferGranularity.height;
                uint32_t depth = queueFamilyProperty.minImageTransferGranularity.depth;
                ::std::cout << "        - Min Image TransferGranularity:    " << width << ", " << height << ", " << depth << ::std::endl;
                ::std::cout << ::std::endl;
            }

            VkSurfaceCapabilitiesKHR surfaceCapabilities;
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities); //TODO: make sure the surface has all capabilities that we need!
            ::std::cout << "     - Surface Capabilities:         " << ::std::endl;
            ::std::cout << "        - minImageCount:            " << surfaceCapabilities.minImageCount << ::std::endl;
            ::std::cout << "        - maxImageCount:            " << surfaceCapabilities.maxImageCount << ::std::endl;
            ::std::cout << "        - currentExtent:            " << surfaceCapabilities.currentExtent.width << "/" << surfaceCapabilities.currentExtent.height << ::std::endl;
            ::std::cout << "        - minImageExtent:           " << surfaceCapabilities.minImageExtent.width << "/" << surfaceCapabilities.minImageExtent.height << ::std::endl;
            ::std::cout << "        - maxImageExtent:           " << surfaceCapabilities.maxImageExtent.width << "/" << surfaceCapabilities.maxImageExtent.height << ::std::endl;
            ::std::cout << "        - maxImageArrayLayers:      " << surfaceCapabilities.maxImageArrayLayers << ::std::endl;
            ::std::cout << "        - supportedTransforms:      " << surfaceCapabilities.supportedTransforms << ::std::endl;
            ::std::cout << "        - currentTransform:         " << surfaceCapabilities.currentTransform << ::std::endl;
            ::std::cout << "        - supportedCompositeAlpha:  " << surfaceCapabilities.supportedCompositeAlpha << ::std::endl;
            ::std::cout << "        - supportedUsageFlags:      " << surfaceCapabilities.supportedUsageFlags << ::std::endl;
            ::std::cout << ::std::endl;

            uint32_t formatCount = 0;
            vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
            ::std::vector<VkSurfaceFormatKHR> surfaceFormats(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, surfaceFormats.data());
            ::std::cout << "     - Format Count: " << formatCount << ::std::endl;
            ::std::cout << ::std::endl;
            formatCount = 0;
            for (VkSurfaceFormatKHR surfaceFormat : surfaceFormats)
            {
                ::std::cout << "        - ID | Format:  " << formatCount++ << " | " << surfaceFormat.format << ::std::endl;
                ::std::cout << ::std::endl;
            }
            
            uint32_t presentationModeCount = 0;
            vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModeCount, nullptr);
            ::std::vector<VkPresentModeKHR> presentationModes(presentationModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModeCount, presentationModes.data());
            ::std::cout << "     - Presentation Mode Count: " << presentationModeCount << ::std::endl;
            ::std::cout << ::std::endl;
            for (uint32_t i = 0; i < presentationModeCount; i++)
            {
                ::std::cout << "       - ID | Mode:  " << i << " | " << presentationModes[i] << ::std::endl;
                ::std::cout << ::std::endl;
            }

        }

    }

#endif

    static void createInstance() {

        //=== LAYERS ===//

        // All enabled Instance Layers
        const ::std::vector<const char*> enabledInstanceLayers = {
            #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
            "VK_LAYER_KHRONOS_validation"
            #endif
        };

        uint32_t layerCount = 0;
        VkResult vkResult = vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
        ASSERT_VKRESULT_THROW("Could not fetch the number of Vulkan instance layers.", vkResult)

        ::std::vector<VkLayerProperties> availableInstanceLayers(layerCount);
        vkResult = vkEnumerateInstanceLayerProperties(&layerCount, availableInstanceLayers.data());
        ASSERT_VKRESULT_THROW("Could not fetch all Vulkan instance layer properties.", vkResult)

        #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
            debugPrintInstanceLayerProperties(availableInstanceLayers);
        #endif
        
        for (const char* layerName : enabledInstanceLayers) {
            bool layerFound = false;

            for (const VkLayerProperties layerProperties : availableInstanceLayers) {
                if (strcmp(layerName, layerProperties.layerName)) {
                    layerFound = true;
                    break;
                }
            }

            if (!layerFound) {
                char* message = nullptr;
                sprintf(message, "%s %s %s",
                    "Could not find required:", layerName, "instance layer for vulkan! Updating graphics drivers and restarting your pc may fix this.");
                throw std::runtime_error(message);
            }
        }

        //=== EXTENSIONS ===//

        uint32_t glfwExtenstionCount = 0;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtenstionCount);      //glfw keeps ownership of this pointer

        uint32_t extensionCount = 0;
        vkResult = vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
        ASSERT_VKRESULT_THROW("Could not fetch the number of Vulkan instance extensions.", vkResult)

        ::std::vector<VkExtensionProperties> availableInstanceExtensions(extensionCount);
        vkResult = vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableInstanceExtensions.data());
        ASSERT_VKRESULT_THROW("Could not fetch all Vulkan instance extension properties.", vkResult)

        for (uint32_t i = 0; i < glfwExtenstionCount; i++) {
            bool extensionFound = false;

            for (const VkExtensionProperties extensionProperties : availableInstanceExtensions) {
                if (strcmp(glfwExtensions[i], extensionProperties.extensionName) == 0) {
                    extensionFound = true;
                    break;
                }
            }

            if (!extensionFound) {
                char* message = nullptr;
                sprintf(message, "%s %s %s",
                    "Could not find required:", glfwExtensions[i], "instance extension for vulkan! Updating graphics drivers and restarting your pc may fix this.");
                throw std::runtime_error(message);
            }
        }

        VkApplicationInfo applicationInfo;
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pNext = nullptr;
        applicationInfo.pApplicationName = CREATO_APPLICATION_NAME;
        applicationInfo.applicationVersion = CREATO_APPLICATION_VERSION;
        applicationInfo.pEngineName = "No Engine";
        applicationInfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);
        applicationInfo.apiVersion = VK_API_VERSION_1_2;

        //=== INSTANCE_CREATE_INFO ===//
        VkInstanceCreateInfo instanceCreateInfo;
        instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCreateInfo.pNext = nullptr;
        instanceCreateInfo.flags = 0;
        instanceCreateInfo.pApplicationInfo = &applicationInfo;
        instanceCreateInfo.enabledLayerCount = enabledInstanceLayers.size();
        instanceCreateInfo.ppEnabledLayerNames = enabledInstanceLayers.data();
        instanceCreateInfo.enabledExtensionCount = glfwExtenstionCount;
        instanceCreateInfo.ppEnabledExtensionNames = glfwExtensions;

        vkResult = vkCreateInstance(&instanceCreateInfo, nullptr, &(g_Application.instance));
        ASSERT_VKRESULT_THROW("Could not create a Vulkan Instance.", vkResult)
    }

    static void onWindowResized(GLFWwindow* gWindow, int newWidth, int newHeight) {

        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        Window* vWindow = windowMap[gWindow];
        VkExtent2D &windowSize = vWindow->windowSize;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(g_Application.physicalDevice, vWindow->surface, &surfaceCapabilities);
        if (windowSize.width > surfaceCapabilities.maxImageExtent.width) {
            newWidth = surfaceCapabilities.maxImageExtent.width;
        }
        if (windowSize.height > surfaceCapabilities.maxImageExtent.height) {
            newHeight = surfaceCapabilities.maxImageExtent.height;
        } 

        windowSize = {(uint32_t) newWidth, (uint32_t) newHeight};

        if (newWidth == 0 || newHeight == 0) {
            return;
        }
        recreateWindow(vWindow);
        drawframe(vWindow);
    }

    static CreatoGraphicsResultInfo* createWindowSurface(const char* title, GLFWwindow* &glfwWindow, VkExtent2D windowSize, VkSurfaceKHR &surface) {

        // disable opengl
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);
        //glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

        //m_WindowSize should always be small enough for the conversion to work.
        glfwWindow = glfwCreateWindow(windowSize.width, windowSize.height, title, nullptr, nullptr);
        glfwSetWindowSizeCallback(glfwWindow, onWindowResized); //TODO make a graphics, input, and ui manager so they dont have to cyclic include themselfes

        VkResult vkResult = glfwCreateWindowSurface(g_Application.instance, glfwWindow, nullptr, &surface);
        ASSERT_VKRESULT_LGR_T(vkResult, LGR_CREATE_ERROR, "Could not create GLFW Window Surface.")
        return LGRI_SUCCESS;
    }

    static VkSampleCountFlagBits getMaxUsableSampleCount() {
        VkPhysicalDeviceProperties physicalDeviceProperties;
        vkGetPhysicalDeviceProperties(g_Application.physicalDevice, &physicalDeviceProperties);

        VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferDepthSampleCounts;
        if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
        if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
        if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
        if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
        if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
        if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }

        return VK_SAMPLE_COUNT_1_BIT;
    }

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-parameter"

    static void pickPhysicalDevice(VkSurfaceKHR &surface) { //TODO add argument of index vector of ids to be skipped (so this can be called again if no good queue was availabel on selected gpu)

        uint32_t physicalDeviceCount = 0;
        VkResult vkResult = vkEnumeratePhysicalDevices(g_Application.instance, &physicalDeviceCount, nullptr);
        ASSERT_VKRESULT_THROW("Could not fetch the number of Vulkan physical devices (graphics cards).", vkResult)

        if (physicalDeviceCount <= 0) {
            throw std::runtime_error("No Vulkan physical devices (e.g. graphics cards) detected! Please make sure you have one and update its drivers!");
        }

        std::vector<VkPhysicalDevice> physicalDevices = ::std::vector<VkPhysicalDevice>(physicalDeviceCount);
        vkResult = vkEnumeratePhysicalDevices(g_Application.instance, &physicalDeviceCount, physicalDevices.data());
        ASSERT_VKRESULT_THROW("Could not fetch the Vulkan properties of all physical devices (graphics cards).", vkResult)
        
        for (uint32_t i = 0; i < physicalDevices.size(); i++) {
            VkPhysicalDeviceProperties physicalDeviceProperties;
            vkGetPhysicalDeviceProperties(physicalDevices[i], &physicalDeviceProperties);
            VkPhysicalDeviceFeatures supportedFeatures;
            vkGetPhysicalDeviceFeatures(physicalDevices[i], &supportedFeatures);
            if (physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && supportedFeatures.samplerAnisotropy) {
                g_Application.physicalDevice = physicalDevices[i];
                g_Application.msaaSampleCount = getMaxUsableSampleCount();
                break;
            }
        }

        #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
            debugPrintPhysicalDeviceProperties(physicalDevices, surface);
        #endif
    }

    #pragma GCC diagnostic pop

    static void createDevice(VkSurfaceKHR &surface) {

        pickPhysicalDevice(surface);

        //select best queueFamilyIndex
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(g_Application.physicalDevice, &queueFamilyCount, nullptr);
        ::std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(g_Application.physicalDevice, &queueFamilyCount, queueFamilyProperties.data());
        for (uint32_t i = 0; i < queueFamilyCount; i++) {
            if (queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                g_Application.queueFamilyIndex = i;
                break;
            }
        }
        
        const float queueProirities[] = {1.0f}; //TODO Research if truely irrelevant, if only one queue exists.

        // (Logical) DeviceQueueCreateInfo
        VkDeviceQueueCreateInfo deviceQueueCreateInfo;
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.pNext = nullptr;
        deviceQueueCreateInfo.flags = 0;
        deviceQueueCreateInfo.queueFamilyIndex = g_Application.queueFamilyIndex;
        deviceQueueCreateInfo.queueCount = 1;
        deviceQueueCreateInfo.pQueuePriorities = queueProirities;

        // (Logical) Device layers
        // const ::std::vector<const char*> deviceLayers = ::std::vector<const char*>(0);
        // vkEnumerateDeviceLayerProperties (and check)

        // (Logical) Device Extensions
        const ::std::vector<const char*> deviceExtensions{
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        //TODO vkEnumerateDeviceExtensionProperties (and check)

        // (Logical) Device Features
        VkPhysicalDeviceFeatures usedFeatures = {}; //Everything in this struct is false
        usedFeatures.samplerAnisotropy = VK_TRUE;

        // Check if the vulkan surface is compatible with our chosen physical device
        VkBool32 surfaceSupport = false;
        VkResult vkResult = vkGetPhysicalDeviceSurfaceSupportKHR(g_Application.physicalDevice, g_Application.queueFamilyIndex, surface, &surfaceSupport);
        ASSERT_VKRESULT_THROW("Could not query if yout gpu supports our Surface (Window).", vkResult)

        if (!surfaceSupport) { //TODO repick physicalDevice?
            throw std::runtime_error("Could not find a GPU with queues that are capable of displaying our surfaces (our windows)");
        }

        // (Logical) DeviceCreateInfo
        VkDeviceCreateInfo deviceCreateInfo;
        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.pNext = nullptr;
        deviceCreateInfo.flags = 0;
        deviceCreateInfo.queueCreateInfoCount = 1;
        deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
        deviceCreateInfo.enabledLayerCount = 0;
        deviceCreateInfo.ppEnabledLayerNames = nullptr;
        deviceCreateInfo.enabledExtensionCount = deviceExtensions.size();
        deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();
        deviceCreateInfo.pEnabledFeatures = &usedFeatures;

        //(Logical) Device
        vkResult = vkCreateDevice(g_Application.physicalDevice, &deviceCreateInfo, nullptr, &g_Application.device); //TODO Retry with other devices
        ASSERT_VKRESULT_THROW("Could not create a logical device.", vkResult)

        //Queues for that logical device (dependent on the deviceQueueCreateInfo)
        vkGetDeviceQueue(g_Application.device, 0, 0, &(g_Application.queue));
    }

    //Reads codepoints of a file at an absolute path.
    static ::std::vector<char> readFileSigned(const ::std::string &path) {

        ::std::ifstream file(path, ::std::ios::binary | ::std::ios::ate);

        if (!file) {
            __debugbreak();
            return ::std::vector<char>(0);
        }

        size_t fileSize = (size_t)file.tellg();     //read file from end (to read what pos the end is)
        ::std::vector<char> fileBuffer(fileSize);
        file.seekg(0);                              //move readpos to start
        file.read(fileBuffer.data(), fileSize);
        file.close();

        return fileBuffer;
    }

    //Reads codepoints of a file at an absolute path.
    static ::std::vector<unsigned char> readFileUnsigned(const ::std::string &path) {

        ::std::ifstream file(path, ::std::ios::binary | ::std::ios::ate);

        if (!file) {
            __debugbreak();
            return ::std::vector<unsigned char>(0);
        }

        size_t fileSize = (size_t)file.tellg();     //read file from end (to read what pos the end is)
        ::std::vector<unsigned char> fileBuffer(fileSize);
        file.seekg(0);                              //move readpos to start
        file.read((char*)fileBuffer.data(), fileSize);
        file.close();

        return fileBuffer;
    }

    static VkPipelineShaderStageCreateInfo createPipelineShaderStage(std::string fileName, VkShaderStageFlagBits stage) {
        std::string path = filesystem::makeAbsoluteProcPath("/shaders/" + fileName);
        VkShaderModule module;

        ::std::vector<char> shaderCode = readFileSigned(path);

        VkShaderModuleCreateInfo shaderCreateInfo;
        shaderCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shaderCreateInfo.pNext = nullptr;
        shaderCreateInfo.flags = 0;
        shaderCreateInfo.codeSize = shaderCode.size();
        shaderCreateInfo.pCode = (uint32_t*)shaderCode.data();

        VkResult vkResult = vkCreateShaderModule(g_Application.device, &shaderCreateInfo, nullptr, &module);
        if (vkResult) {
            char* message = nullptr;
            sprintf(message, "%s %s %s %d", "Could not create Shader Module:", fileName.c_str(), "VkResult was:", vkResult);
            throw std::runtime_error(message);
        }

        VkPipelineShaderStageCreateInfo createInfo;
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        createInfo.pNext = nullptr;
        createInfo.flags = 0;
        createInfo.stage = stage;
        createInfo.module = module;
        createInfo.pName = "main";
        createInfo.pSpecializationInfo = nullptr;

        return createInfo;
    }

    static VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
        for (VkFormat format : candidates) {
            VkFormatProperties props;
            vkGetPhysicalDeviceFormatProperties(g_Application.physicalDevice, format, &props);

            if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
                return format;
            } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
                return format;
            }
        }
        throw std::runtime_error("failed to find supported format!"); //Critical error
    }

    static VkFormat findDepthFormat() { //TODO only call once
        return findSupportedFormat(
            {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT},
            VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
        );
    }

    static void createRenderPass() {

        VkAttachmentDescription colorAttachment;
        colorAttachment.flags = 0;
        colorAttachment.format = g_Application.colorFormat;
        colorAttachment.samples = g_Application.msaaSampleCount;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        //Could be an Array
        VkAttachmentReference colorAttachmentReference;
        colorAttachmentReference.attachment = 0;
        colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription colorAttachmentResolve{};
        colorAttachmentResolve.format = g_Application.colorFormat;
        colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference colorAttachmentResolveReference;
        colorAttachmentResolveReference.attachment = 2;
        colorAttachmentResolveReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription depthAttachment{};
        depthAttachment.flags = 0; 
        depthAttachment.format = findDepthFormat();
        depthAttachment.samples = g_Application.msaaSampleCount;
        depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depthAttachmentReference{};
        depthAttachmentReference.attachment = 1;
        depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpassDescription;
        subpassDescription.flags = 0;
        subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;             //TODO sync ????
        subpassDescription.inputAttachmentCount = 0;
        subpassDescription.pInputAttachments = nullptr;
        subpassDescription.colorAttachmentCount = 1;
        subpassDescription.pColorAttachments = &colorAttachmentReference;
        subpassDescription.pResolveAttachments = &colorAttachmentResolveReference;
        subpassDescription.pDepthStencilAttachment = &depthAttachmentReference;
        subpassDescription.preserveAttachmentCount = 0;
        subpassDescription.pPreserveAttachments = nullptr;

        VkSubpassDependency subpassDependency;
        subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        subpassDependency.dstSubpass = 0;
        subpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDependency.srcAccessMask = 0;
        subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT; //VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
        subpassDependency.dependencyFlags = 0;

        std::array<VkAttachmentDescription, 3> attachments = {colorAttachment, depthAttachment, colorAttachmentResolve};

        VkRenderPassCreateInfo renderPassCreateInfo;
        renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassCreateInfo.pNext = nullptr;
        renderPassCreateInfo.flags = 0;
        renderPassCreateInfo.attachmentCount = attachments.size();
        renderPassCreateInfo.pAttachments = attachments.data();                  //!Description must be used here
        renderPassCreateInfo.subpassCount = 1;
        renderPassCreateInfo.pSubpasses = &subpassDescription;
        renderPassCreateInfo.dependencyCount = 1;
        renderPassCreateInfo.pDependencies = &subpassDependency;

        VkResult vkResult = vkCreateRenderPass(g_Application.device, &renderPassCreateInfo , nullptr, &(g_Application.renderPass));
        ASSERT_VKRESULT_THROW("Could not create Renderpass.", vkResult)
    }

    static void createDescriptorSetLayout() {

        VkDescriptorSetLayoutBinding uniformLayoutBinding;
        uniformLayoutBinding.binding = 0;
        uniformLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        uniformLayoutBinding.descriptorCount = 1;
        uniformLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        uniformLayoutBinding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutBinding samplerLayoutBinding{};
        samplerLayoutBinding.binding = 1;
        samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        samplerLayoutBinding.descriptorCount = 1;
        samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        samplerLayoutBinding.pImmutableSamplers = nullptr;

        std::array<VkDescriptorSetLayoutBinding, 2> bindings = {uniformLayoutBinding, samplerLayoutBinding};

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
        descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorSetLayoutCreateInfo.pNext = nullptr;
        descriptorSetLayoutCreateInfo.flags = 0;
        descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(bindings.size());
        descriptorSetLayoutCreateInfo.pBindings = bindings.data();

        VkResult vkResult = vkCreateDescriptorSetLayout(g_Application.device, &descriptorSetLayoutCreateInfo, nullptr, &(g_Application.descriptorSetLayout));
        ASSERT_VKRESULT_THROW("Could not create a DescriptorSetLayout.", vkResult)
    }

    static void createPipelines() {

        ::std::array<VkPipelineShaderStageCreateInfo, 2> pipelineShaderStages = {
                createPipelineShaderStage("scene_vert.spv", VK_SHADER_STAGE_VERTEX_BIT),
                createPipelineShaderStage("scene_frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT)
        };

        VkVertexInputBindingDescription vertexBindingDescription = Vertex::getBindingDescription();
        std::array<VkVertexInputAttributeDescription, 2> vertexAttributeDescriptions = Vertex::getAttributeDescriptions();

        VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo;
        pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        pipelineVertexInputStateCreateInfo.pNext = nullptr;
        pipelineVertexInputStateCreateInfo.flags = 0;
        pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
        pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexBindingDescription;
        pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = vertexAttributeDescriptions.size();
        pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = vertexAttributeDescriptions.data();

        VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo;
        pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        pipelineInputAssemblyStateCreateInfo.pNext = nullptr;
        pipelineInputAssemblyStateCreateInfo.flags = 0;
        pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST; //VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP for squares
        pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

        /*
        VkViewport viewport;
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = ?;
        viewport.height = ?;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor; //aka inverse scissor
        scissor.offset = { 0, 0};
        scissor.extent = ?;
        */

        VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo;
        pipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        pipelineViewportStateCreateInfo.pNext = nullptr;
        pipelineViewportStateCreateInfo.flags = 0;
        pipelineViewportStateCreateInfo.viewportCount = 1;
        //pipelineViewportStateCreateInfo.pViewports = &viewport;
        pipelineViewportStateCreateInfo.scissorCount = 1;
        //pipelineViewportStateCreateInfo.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo;
        pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        pipelineRasterizationStateCreateInfo.pNext = nullptr;
        pipelineRasterizationStateCreateInfo.flags = 0;
        pipelineRasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
        pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
        pipelineRasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
        pipelineRasterizationStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
        pipelineRasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        pipelineRasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
        pipelineRasterizationStateCreateInfo.depthBiasConstantFactor = 0.0f;
        pipelineRasterizationStateCreateInfo.depthBiasClamp = 0.0f;
        pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor = 0.0f;
        pipelineRasterizationStateCreateInfo.lineWidth = 1.0f;

        //Antialiasing
        VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo;
        pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        pipelineMultisampleStateCreateInfo.pNext = nullptr;
        pipelineMultisampleStateCreateInfo.flags = 0;
        pipelineMultisampleStateCreateInfo.rasterizationSamples = g_Application.msaaSampleCount;
        pipelineMultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
        pipelineMultisampleStateCreateInfo.minSampleShading = 1.0f;
        pipelineMultisampleStateCreateInfo.pSampleMask = nullptr;
        pipelineMultisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
        pipelineMultisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

        VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState;
        pipelineColorBlendAttachmentState.blendEnable = VK_TRUE;
        pipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        pipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
        pipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        pipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
        pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo;
        pipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        pipelineColorBlendStateCreateInfo.pNext = nullptr;
        pipelineColorBlendStateCreateInfo.flags = 0;
        pipelineColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
        pipelineColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_NO_OP;
        pipelineColorBlendStateCreateInfo.attachmentCount = 1;
        pipelineColorBlendStateCreateInfo.pAttachments = &pipelineColorBlendAttachmentState;
        pipelineColorBlendStateCreateInfo.blendConstants[0] = 0.0f;
        pipelineColorBlendStateCreateInfo.blendConstants[1] = 0.0f;
        pipelineColorBlendStateCreateInfo.blendConstants[2] = 0.0f;
        pipelineColorBlendStateCreateInfo.blendConstants[3] = 0.0f;

        ::std::vector<VkDynamicState> dynamicStates = {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_SCISSOR
        };

        VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo;
        pipelineDynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        pipelineDynamicStateCreateInfo.pNext = nullptr;
        pipelineDynamicStateCreateInfo.flags = 0;
        pipelineDynamicStateCreateInfo.dynamicStateCount = 2;
        pipelineDynamicStateCreateInfo.pDynamicStates = dynamicStates.data();

        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
        pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.pNext = nullptr;
        pipelineLayoutCreateInfo.flags = 0;
        pipelineLayoutCreateInfo.setLayoutCount = 1;
        pipelineLayoutCreateInfo.pSetLayouts = &(g_Application.descriptorSetLayout);
        pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
        pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

        VkResult vkResult = vkCreatePipelineLayout(g_Application.device, &pipelineLayoutCreateInfo, nullptr, &(g_Application.pipelineLayout));
        ASSERT_VKRESULT_THROW("Could not create PipelineLayout.", vkResult)

        VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo;
        depthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilStateCreateInfo.pNext = nullptr;
        depthStencilStateCreateInfo.flags = 0;
        depthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
        depthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
        depthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
        depthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
        depthStencilStateCreateInfo.front = {};
        depthStencilStateCreateInfo.back = {};
        depthStencilStateCreateInfo.minDepthBounds = 0.0f; // Optional;
        depthStencilStateCreateInfo.maxDepthBounds = 1.0f; // Optional;

        VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo;
        graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        graphicsPipelineCreateInfo.pNext = nullptr;
        graphicsPipelineCreateInfo.flags = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
        graphicsPipelineCreateInfo.stageCount = 2;
        graphicsPipelineCreateInfo.pStages = pipelineShaderStages.data();
        graphicsPipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
        graphicsPipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;
        graphicsPipelineCreateInfo.pTessellationState = nullptr;
        graphicsPipelineCreateInfo.pViewportState = &pipelineViewportStateCreateInfo;
        graphicsPipelineCreateInfo.pRasterizationState = &pipelineRasterizationStateCreateInfo;
        graphicsPipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
        graphicsPipelineCreateInfo.pDepthStencilState = &depthStencilStateCreateInfo;
        graphicsPipelineCreateInfo.pColorBlendState = &pipelineColorBlendStateCreateInfo;
        graphicsPipelineCreateInfo.pDynamicState = &pipelineDynamicStateCreateInfo;
        graphicsPipelineCreateInfo.layout = g_Application.pipelineLayout;
        graphicsPipelineCreateInfo.renderPass = g_Application.renderPass;
        graphicsPipelineCreateInfo.subpass = 0;
        graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
        graphicsPipelineCreateInfo.basePipelineIndex = -1;

        //TODO PipelineCache
        vkResult = vkCreateGraphicsPipelines(g_Application.device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &(g_Application.scenePipeline));
        ASSERT_VKRESULT_THROW("Could not create Pipeline.", vkResult)

        for (auto& pipelineShaderStage : pipelineShaderStages) {
            vkDestroyShaderModule(g_Application.device, pipelineShaderStage.module, nullptr);
        }

        // All pipelines created below will be derivatives

		graphicsPipelineCreateInfo.flags = VK_PIPELINE_CREATE_DERIVATIVE_BIT;

		// Base pipeline will be our first created pipeline
		graphicsPipelineCreateInfo.basePipelineHandle = g_Application.scenePipeline;
		// It's only allowed to either use a handle or index for the base pipeline
		// As we use the handle, we must set the index to -1 (see section 9.5 of the specification)
		graphicsPipelineCreateInfo.basePipelineIndex = -1;

        pipelineShaderStages[0] = createPipelineShaderStage("text_vert.spv", VK_SHADER_STAGE_VERTEX_BIT);
        pipelineShaderStages[1] = createPipelineShaderStage("text_frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT);

		//Text Rendering Pipeline
		vkCreateGraphicsPipelines(g_Application.device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &g_Application.textPipeline);
    }

    static void createCommandPool() {

        VkCommandPoolCreateInfo commandPoolCreateInfo;
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.pNext = nullptr;
        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        commandPoolCreateInfo.queueFamilyIndex = g_Application.queueFamilyIndex;

        VkResult vkResult = vkCreateCommandPool(g_Application.device, &commandPoolCreateInfo , nullptr, &(g_Application.commandPool));
        ASSERT_VKRESULT_THROW("Could not create a CommandPool.", vkResult)
    }

    void init() {

        // start glfw
        glfwInit();

        createInstance(); //TODO debug callbacks

        // A dummy window has to be created in order to choose a compatible device.
        // API-Info: You can change the dummy window to show a loading icon (maybe with transparancy?) and keep it until your application has fully loaded.
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        GLFWwindow* dummyWindow = glfwCreateWindow(100, 100, "dummy", nullptr, nullptr);
        VkSurfaceKHR dummySurface;
        VkResult vkResult = glfwCreateWindowSurface(g_Application.instance, dummyWindow, nullptr, &dummySurface);
        if (vkResult) {
            char* message = nullptr;
            sprintf(message, "%s %d", "Could not create a Surface. VkResult was:", vkResult);
            throw std::runtime_error(message);
        }

        createDevice(dummySurface);

        // Destroy the dummy window
        vkDestroySurfaceKHR(g_Application.instance, dummySurface, nullptr);
        glfwDestroyWindow(dummyWindow);
        glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);

        createRenderPass();

        // Model and ViewProjection (Must be called before Pipeline)
        createDescriptorSetLayout();

        createPipelines();

        createCommandPool();
    }

    static uint32_t findMemoryTypeIndex(uint32_t typeFilter, VkMemoryMapFlags properties) {

        VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
        vkGetPhysicalDeviceMemoryProperties(g_Application.physicalDevice, &physicalDeviceMemoryProperties); //TODO check index

        for (uint32_t i = 0; i < physicalDeviceMemoryProperties.memoryTypeCount; i++) {
            if ((typeFilter & (1 << i)) && (physicalDeviceMemoryProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }
        }
        throw std::runtime_error("Found no correct memory type!");
    }

    static CreatoGraphicsResultInfo* createImage(uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits sampleCount, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory) {

        VkImageCreateInfo imageCreateInfo;
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.pNext = nullptr;
        imageCreateInfo.flags = 0;
        imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
        imageCreateInfo.format = format;
        imageCreateInfo.extent.width = width;
        imageCreateInfo.extent.height = height;
        imageCreateInfo.extent.depth = 1;
        imageCreateInfo.mipLevels = mipLevels;
        imageCreateInfo.arrayLayers = 1;
        imageCreateInfo.samples = sampleCount;
        imageCreateInfo.tiling = tiling;
        imageCreateInfo.usage = usage;
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.queueFamilyIndexCount = 0;
        imageCreateInfo.pQueueFamilyIndices = nullptr;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        VkResult vkResult = vkCreateImage(g_Application.device, &imageCreateInfo, nullptr, &(image));
        ASSERT_VKRESULT_LGR(vkResult, LGR_CREATE_ERROR)

        VkMemoryRequirements memRequirements;
        vkGetImageMemoryRequirements(g_Application.device, image, &memRequirements);

        VkMemoryAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.pNext = nullptr;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = findMemoryTypeIndex(memRequirements.memoryTypeBits, properties);

        vkResult = vkAllocateMemory(g_Application.device, &allocInfo, nullptr, &imageMemory);
        ASSERT_VKRESULT_LGR(vkResult, LGR_ALLOC_ERROR)

        vkResult = vkBindImageMemory(g_Application.device, image, imageMemory, 0);
        ASSERT_VKRESULT_LGR(vkResult, LGR_BIND_ERROR)

        return LGRI_SUCCESS;
    }

    static CreatoGraphicsResultInfo* createImageView(VkImage image, VkImageView& imageView, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) {
        VkImageViewCreateInfo viewInfo{};
        viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewInfo.image = image;
        viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewInfo.format = format;
        viewInfo.subresourceRange.aspectMask = aspectFlags;
        viewInfo.subresourceRange.baseMipLevel = 0;
        viewInfo.subresourceRange.levelCount = mipLevels;
        viewInfo.subresourceRange.baseArrayLayer = 0;
        viewInfo.subresourceRange.layerCount = 1;

        VkResult vkResult = vkCreateImageView(g_Application.device, &viewInfo, nullptr, &imageView);
        ASSERT_VKRESULT_LGR(vkResult, LGR_ERROR)

        return LGRI_SUCCESS;
    }

    static void createColorBuffer(VkExtent2D windowExtent, VkImage& colorImage, VkDeviceMemory& colorImageMemory, VkImageView& colorImageView) { //Multisampling
        createImage(windowExtent.width, windowExtent.height, 1, g_Application.msaaSampleCount, g_Application.colorFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, colorImage, colorImageMemory);
        createImageView(colorImage, colorImageView, g_Application.colorFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
        //TODO Errorhandling
    }

    static VkCommandBuffer beginOneTimeCommandBuffer(CreatoGraphicsResultInfo* lgResInfo) {

        if (lgResInfo) {
            lgResInfo->lgResult = LGR_SUCCESS;
        }

        VkCommandBufferAllocateInfo commandBufferAllocateInfo;
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.pNext = nullptr;
        commandBufferAllocateInfo.commandPool = g_Application.commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        VkResult vkResult = vkAllocateCommandBuffers(g_Application.device, &commandBufferAllocateInfo, &commandBuffer);
        ASSERT_VKRESULT_LGRI_LGR_T(vkResult, lgResInfo, LGR_ALLOC_ERROR, "")
        if (vkResult) {
            return commandBuffer;
        }

        VkCommandBufferBeginInfo commandBufferBeginInfo;
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        commandBufferBeginInfo.pNext = nullptr;
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        commandBufferBeginInfo.pInheritanceInfo = nullptr;

        vkResult = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
        ASSERT_VKRESULT_LGRI_LGR_T(vkResult, lgResInfo, LGR_BEGIN_ERROR, "")

        return commandBuffer;
    }
    
    static CreatoGraphicsResultInfo* endOneTimeCommandBuffer(VkCommandBuffer &commandBuffer) {
        VkResult vkResult = vkEndCommandBuffer(commandBuffer);
        ASSERT_VKRESULT_LGR_T(vkResult, LGR_ERROR, "Could not end command buffer recording.");

        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResult = vkQueueSubmit(g_Application.queue, 1, &submitInfo, VK_NULL_HANDLE); //TODO Search transfer queue (ep 99, min 23)
        ASSERT_VKRESULT_LGR_T(vkResult, LGR_ERROR, "Could not submit Queue.");

        vkQueueWaitIdle(g_Application.queue);

        vkFreeCommandBuffers(g_Application.device, g_Application.commandPool, 1, &commandBuffer);

        return LGRI_SUCCESS;
    }

    static bool hasStencilComponent(VkFormat format) {
        return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
    }

    static void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels) {
        //TODO: optimize the shit out of this pls
        VkCommandBuffer commandBuffer = beginOneTimeCommandBuffer(nullptr); //TODO

        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout; 
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = image;

        if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

            if (hasStencilComponent(format)) {
                barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
            }
        } else {
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        }

        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = mipLevels;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;
        barrier.srcAccessMask = 0; // TODO
        barrier.dstAccessMask = 0; // TODO

        VkPipelineStageFlags sourceStage;
        VkPipelineStageFlags destinationStage;

        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

        } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

        } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;

        } else {
            throw std::invalid_argument("unsupported layout transition!");
        }

        vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr,0, nullptr,1, &barrier);

        endOneTimeCommandBuffer(commandBuffer);
    }

    static void createDepthBuffer(VkExtent2D windowExtent, VkImage& depthImage, VkDeviceMemory& depthImageMemory, VkImageView& depthImageView) {

        VkFormat format = findDepthFormat();

        createImage(windowExtent.width, windowExtent.height, 1, g_Application.msaaSampleCount, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
        createImageView(depthImage, depthImageView, format, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
        //TODO ErrorHandling

        transitionImageLayout(depthImage, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);
    }
    
    static void createOrRecreateFrames(VkSurfaceKHR &surface, VkSwapchainKHR &swapchain, std::vector<Frame> &frames, VkImageView colorImageView, VkImageView depthImageView, VkExtent2D windowSize) {
        
        bool b_IsFirstCreation = (swapchain == VK_NULL_HANDLE);

        //SwapchainCreateInfo
        VkSwapchainCreateInfoKHR swapchainCreateInfo;
        swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        swapchainCreateInfo.pNext = nullptr;
        swapchainCreateInfo.flags = 0;
        swapchainCreateInfo.surface = surface;
        swapchainCreateInfo.minImageCount = 3;
        swapchainCreateInfo.imageFormat = g_Application.colorFormat;                    //TODO check if valid
        swapchainCreateInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;        //TODO check if valid
        swapchainCreateInfo.imageExtent = windowSize;  
        swapchainCreateInfo.imageArrayLayers = 1;                                       //TODO Research
        swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;               //TODO check if valid
        swapchainCreateInfo.queueFamilyIndexCount = 0;  
        swapchainCreateInfo.pQueueFamilyIndices = nullptr;  
        swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;         //API-Info GLFW manages window transparancy, use glfw window hints.
        swapchainCreateInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;                  //TODO check if valid
        swapchainCreateInfo.clipped = VK_TRUE;                                          //TODO maybe false for shaders
        swapchainCreateInfo.oldSwapchain = swapchain;                                   //TODO set when resize??

        VkResult vkResult = vkCreateSwapchainKHR(g_Application.device, &swapchainCreateInfo, nullptr, &(swapchain));
        ASSERT_VKRESULT_THROW("Could not create Swapchain.", vkResult);

        uint32_t swapchainImageCount = 0;
        vkResult = vkGetSwapchainImagesKHR(g_Application.device, swapchain, &(swapchainImageCount), nullptr);
        ASSERT_VKRESULT_THROW("Could not fetch swapchain image count.", vkResult);

        std::vector<VkImage> swapchainImages(swapchainImageCount);
        vkResult = vkGetSwapchainImagesKHR(g_Application.device, swapchain, &swapchainImageCount, swapchainImages.data());
        ASSERT_VKRESULT_THROW("Could not fetch all swapchain images.", vkResult);

        VkImageViewCreateInfo imageViewCreateInfo;
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.pNext = nullptr;
        imageViewCreateInfo.flags = 0;
        //imageViewCreateInfo.image = swapchainImages[0];                             //start position 0
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = g_Application.colorFormat;                      //TODO check if valid
        imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;            //API-Info swap colors
        imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;                      //TODO make mipmaps
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;                    //3D images (dont use)
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        VkCommandBufferAllocateInfo commandBufferAllocateInfo;
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.pNext = nullptr;
        commandBufferAllocateInfo.commandPool = g_Application.commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;

        if (b_IsFirstCreation) {
            frames = std::vector<Frame>(swapchainImageCount);
        }

        for (uint32_t i = 0; i < swapchainImageCount; i++) {

            imageViewCreateInfo.image = swapchainImages[i];  

            vkResult = vkCreateImageView(g_Application.device, &imageViewCreateInfo, nullptr, &(frames[i].imageView));
            ASSERT_VKRESULT_THROW("Could not create VkImageView for swapchainImage.", vkResult);

            std::array<VkImageView, 3> attachments = {
                colorImageView,
                depthImageView,
                frames[i].imageView
            };

            VkFramebufferCreateInfo frameBufferCreateInfo;
            frameBufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            frameBufferCreateInfo.pNext = nullptr;
            frameBufferCreateInfo.flags = 0;
            frameBufferCreateInfo.renderPass = g_Application.renderPass;
            frameBufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            frameBufferCreateInfo.pAttachments = attachments.data();
            frameBufferCreateInfo.width = windowSize.width;
            frameBufferCreateInfo.height = windowSize.height;
            frameBufferCreateInfo.layers = 1;

            VkResult vkResult = vkCreateFramebuffer(g_Application.device, &frameBufferCreateInfo, nullptr, &(frames[i].frameBuffer));
            ASSERT_VKRESULT_THROW("Could not create VkFramebuffer for swapchainImage.", vkResult);

            vkResult = vkAllocateCommandBuffers(g_Application.device, &commandBufferAllocateInfo, &(frames[i].commandBuffer));
            ASSERT_VKRESULT_THROW("Could not allocate VkCommandBuffers for swapchainImage.", vkResult);
        }            
    }

    static CreatoGraphicsResultInfo* recordCommandBuffers(Window* p_Window) { //TODO ErrorHandling
        VkCommandBufferBeginInfo commandBufferBeginInfo;
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        commandBufferBeginInfo.pNext = nullptr;
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        commandBufferBeginInfo.pInheritanceInfo = nullptr;                              //Not used since all commandbuffers (only one) are VK_COMMAND_BUFFER_LEVEL_PRIMARY

        for (uint32_t i = 0; i < p_Window->frames.size(); i++) {

            VkCommandBuffer &commandBuffer = p_Window->frames[i].commandBuffer;
            VkResult vkResult = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_THROW_ERROR, "Could not begin command buffer recording for frame rendering.")

            std::array<VkClearValue, 2> clearValues{};
            clearValues[0].color = {{0.0f, 0.0f, 0.0f, 0.0f}};
            clearValues[1].depthStencil = {1.0f, 0};

            VkRenderPassBeginInfo renderPassBeginInfo;
            renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBeginInfo.pNext = nullptr;
            renderPassBeginInfo.renderPass = g_Application.renderPass;
            renderPassBeginInfo.framebuffer = p_Window->frames[i].frameBuffer;
            renderPassBeginInfo.renderArea.offset = {0, 0};
            renderPassBeginInfo.renderArea.extent = p_Window->windowSize;
            renderPassBeginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
            renderPassBeginInfo.pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            VkViewport viewport;
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = p_Window->windowSize.width;
            viewport.height = p_Window->windowSize.height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;

            vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

            VkRect2D scissor;
            scissor.offset = {0, 0};
            scissor.extent = p_Window->windowSize;

            vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

            std::vector<VkDeviceSize> offsets = {0};

            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.textPipeline);

            for (const auto& p_Model : p_Window->opaqueInterfaceRenderList) {
                vkCmdBindVertexBuffers(commandBuffer, 0, 1, &(p_Model->p_Mesh->vertexBufferCase.buffer), offsets.data());
                vkCmdBindIndexBuffer(commandBuffer, p_Model->p_Mesh->indexBufferCase.buffer, 0, VK_INDEX_TYPE_UINT32);

                vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.pipelineLayout, 0, 1, &(p_Model->descriptorSet), 0, nullptr);

                //vkCmdDraw(commandBuffers[i], vertices.size(), 1, 0, 0);
                vkCmdDrawIndexed(commandBuffer, p_Model->p_Mesh->v_IndexList.size(), 1, 0, 0, 0);
            }

            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.scenePipeline);

            for (const auto& p_Model : p_Window->sceneRenderList) {
                vkCmdBindVertexBuffers(commandBuffer, 0, 1, &(p_Model->p_Mesh->vertexBufferCase.buffer), offsets.data());
                vkCmdBindIndexBuffer(commandBuffer, p_Model->p_Mesh->indexBufferCase.buffer, 0, VK_INDEX_TYPE_UINT32);

                vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.pipelineLayout, 0, 1, &(p_Model->descriptorSet), 0, nullptr);

                //vkCmdDraw(commandBuffers[i], vertices.size(), 1, 0, 0);
                vkCmdDrawIndexed(commandBuffer, p_Model->p_Mesh->v_IndexList.size(), 1, 0, 0, 0);
            }

            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.textPipeline);

            for (const auto& p_Model : p_Window->transparentInterfaceRenderList) {
                vkCmdBindVertexBuffers(commandBuffer, 0, 1, &(p_Model->p_Mesh->vertexBufferCase.buffer), offsets.data());
                vkCmdBindIndexBuffer(commandBuffer, p_Model->p_Mesh->indexBufferCase.buffer, 0, VK_INDEX_TYPE_UINT32);

                vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, g_Application.pipelineLayout, 0, 1, &(p_Model->descriptorSet), 0, nullptr);

                //vkCmdDraw(commandBuffers[i], vertices.size(), 1, 0, 0);
                vkCmdDrawIndexed(commandBuffer, p_Model->p_Mesh->v_IndexList.size(), 1, 0, 0, 0);
            }

            vkCmdEndRenderPass(commandBuffer);

            vkResult = vkEndCommandBuffer(commandBuffer);
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_THROW_ERROR, "Could not end command buffer recording for frame rendering.")
        }
        return LGRI_SUCCESS;
    }

    static CreatoGraphicsResultInfo* createSyncSets(std::vector<FrameSyncSet>& syncSets, uint32_t syncSetCount) {

        syncSets = std::vector<FrameSyncSet>(syncSetCount);

        VkSemaphoreCreateInfo semaphoreCreateInfo;
        semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreCreateInfo.pNext = nullptr;
        semaphoreCreateInfo.flags = 0;

        VkFenceCreateInfo fenceCreateInfo{};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreateInfo.pNext = nullptr;
        fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for (FrameSyncSet &syncSet : syncSets) {
            VkResult vkResult = vkCreateSemaphore(g_Application.device, &semaphoreCreateInfo, nullptr, &(syncSet.imageAquiredSemaphore));
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_CREATE_ERROR, "Could not create semaphore (gSemaphoreImageAvailable).")

            vkResult = vkCreateSemaphore(g_Application.device, &semaphoreCreateInfo, nullptr, &(syncSet.renderingCompleteSemaphore));
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_CREATE_ERROR, "Could not create semaphore (gSemaphoreRenderingdone).")

            vkResult = vkCreateFence(g_Application.device, &fenceCreateInfo, nullptr, &(syncSet.inFlightFence));
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_CREATE_ERROR, "Could not create fence (inFlightFence).")
        }
        return LGRI_SUCCESS;
    }

    Window* createWindow(const char* title, Node* root) {

        Window* p_Window = new Window();

        createWindowSurface(title, p_Window->glfwWindow, p_Window->windowSize, p_Window->surface);

        windowMap[p_Window->glfwWindow] = p_Window;

        createColorBuffer(p_Window->windowSize, p_Window->colorImage, p_Window->colorImageMemory, p_Window->colorImageView);
        createDepthBuffer(p_Window->windowSize, p_Window->depthImage, p_Window->depthImageMemory, p_Window->depthImageView);
        createOrRecreateFrames(p_Window->surface, p_Window->swapchain, p_Window->frames, p_Window->colorImageView, p_Window->depthImageView, p_Window->windowSize);
        recordCommandBuffers(p_Window);
        createSyncSets(p_Window->syncSets, g_Application.maxInFlightFrames);

        if (root) {
            p_Window->root = root;
            root->activate(p_Window);
        } else {
            p_Window->root = createNode();
            p_Window->root->activate(p_Window); //TODO make that childs will be auto activated when inserted if parent is activated
        }

        return p_Window;
    }

    void changeWindowRoot(Window* p_Window, Node* newRoot) { //TODO nullptr checking in debug mode
        p_Window->root->deactivate();
        p_Window->root = newRoot;
        p_Window->root->activate(p_Window);
    }

    void eraseModelFromRenderLists(Model* p_Model) {
        p_Model->p_Window->sceneRenderList.erase(p_Model);
        p_Model->p_Window->opaqueInterfaceRenderList.erase(p_Model);
        p_Model->p_Window->transparentInterfaceRenderList.erase(p_Model);
        p_Model->p_Window->b_ReRecordCommandBuffers = true;
    }

    void destroyWindow(Window* p_Window) {

        vkDeviceWaitIdle(g_Application.device);

        windowMap.erase(p_Window->glfwWindow);

        auto set = p_Window->sceneRenderList;
        for (const auto& p_Model : set) {
            destroyModel(p_Model);
        }

        for (uint32_t i = 0; i < p_Window->syncSets.size(); i++) {
            vkDestroySemaphore(g_Application.device, p_Window->syncSets[i].imageAquiredSemaphore, nullptr);
            vkDestroySemaphore(g_Application.device, p_Window->syncSets[i].renderingCompleteSemaphore, nullptr);
            vkDestroyFence(g_Application.device, p_Window->syncSets[i].inFlightFence, nullptr);
        }

        for (uint32_t i = 0; i < p_Window->frames.size(); i++) {
            vkFreeCommandBuffers(g_Application.device, g_Application.commandPool, 1, &(p_Window->frames[i].commandBuffer));
            vkDestroyFramebuffer(g_Application.device, p_Window->frames[i].frameBuffer, nullptr);
            vkDestroyImageView(g_Application.device, p_Window->frames[i].imageView, nullptr);
            //vkDestroyFence(g_Application.device, vWindow.frames[i].frameFence, nullptr); (They are just pointers to the already destroyed syncSet fences!)
        }

        vkDestroyImageView(g_Application.device, p_Window->depthImageView, nullptr);
        vkDestroyImage(g_Application.device, p_Window->depthImage, nullptr);
        vkFreeMemory(g_Application.device, p_Window->depthImageMemory, nullptr);

        vkDestroyImageView(g_Application.device, p_Window->colorImageView, nullptr);
        vkDestroyImage(g_Application.device, p_Window->colorImage, nullptr);
        vkFreeMemory(g_Application.device, p_Window->colorImageMemory, nullptr);

        vkDestroySwapchainKHR(g_Application.device, p_Window->swapchain, nullptr);
        vkDestroySurfaceKHR(g_Application.instance, p_Window->surface, nullptr);

        glfwDestroyWindow(p_Window->glfwWindow);

        delete p_Window;
    }

#pragma region /* ===== Nodes ===== */

    Node::~Node() {
        if (parent) {
            detachFromParent();
        }
    };

    void Node::destroy(bool recursive) {
        if (recursive) {
            for (auto& child : children) {
                child->destroy();
            }
        }
        delete this;
    }

    void Node::activate(Window* p_Window, bool recursive) {
        onActivate(p_Window);
        if (recursive) {
            for (auto& child : children) {
                child->activate(p_Window);
            }
        }
    }

    void Node::deactivate(bool recursive) {
        onDeactivate();
        if (recursive) {
            for (auto& child : children) {
                child->deactivate();
            }
        }
    }

    void Node::show(bool recursive) {
        visible = true;
        onShow();

        if (recursive) {
            for (auto& child : children) {
                child->show();
            }
        }
    }

    void Node::hide(bool recursive) {
        visible = false;
        onHide();

        if (recursive) {
            for (auto& child : children) {
                child->hide();
            }
        }
    }

    // Use this function to insert nodes in a scene tree.
    // DO NOT USE IT TO CHANGE A NODE'S POSITION IN THE SCENE TREE, USE changeParent FOR THAT.
    // Calls node->onAttachedToParent(), and onAttachChild(node) in this order.
    void Node::attachChild(Node* node) {
        node->parent = this;
        children.insert(node);

        node->onAttachedToParent();
        onAttachChild(node);
    };

    // Does not delete the given node
    // Calls node->onDetachedFromParent(nullptr), and onDetachChild(node) in this order.
    void Node::detachChild(Node* node) {
        node->parent = nullptr;
        children.erase(node);

        node->onDetachedFromParent(this);
        onDetachChild(node);
    };

    // Only use this when changing a node from one parent to another. Any nullptr will throw!
    // Calls parent->detachChild(this), and newParent->attachChild(this) in this order.
    void Node::changeParent(Node* newParent) {
        parent->detachChild(this);
        newParent->attachChild(this);
    }

    void Node::detachFromParent() {
        parent->detachChild(this);
    }

    Node* createNode() {
        return new Node();
    }

    void destroyNode(Node* node, bool recursive) {
        node->destroy(recursive);
    }

#pragma endregion

    static void updateModelViewProjection(Model* p_Model) {

        UniformBufferObject ubo;

        float ratio = ((float)p_Model->p_Window->windowSize.width) / ((float)p_Model->p_Window->windowSize.height);

        ubo.model = glm::translate(glm::mat4(1.0f), p_Model->position) * glm::toMat4(p_Model->rotation) * glm::scale(glm::mat4(1.0f), p_Model->scale);
        glm::mat4 view = glm::lookAt(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)); //Z-Axis is up?
        glm::mat4 projection = ::glm::perspective(glm::radians(40.0f), ratio, 0.01f, 100.0f);
        projection[1][1] *= -1; //flip y axis

        ubo.viewProjection = projection * view;

        void* rawData;

        #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
        VkResult vkResult = vkMapMemory(g_Application.device, p_Model->uniformBufferCase.memory, 0, sizeof(UniformBufferObject), 0, &rawData);
        ASSERT_VKRESULT_THROW("Could not map memory while updating a Model ubo buffer.", vkResult)
        #else
        vkMapMemory(g_Application.device, p_Model->uniformBufferCase.memory, 0, sizeof(UniformBufferObject), 0, &rawData);
        #endif

        memcpy(rawData, &ubo, sizeof(UniformBufferObject));
        vkUnmapMemory(g_Application.device, p_Model->uniformBufferCase.memory);

    }

    //Camera views stuff from below, 
    static void updateModelViewUIProjection(Model* p_Model) {

        UniformBufferObject ubo;

        glm::mat4 zInversMatrix = glm::mat4( //Inverse Z-Position of vertices in meshes.
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0,-1, 0,
            0, 0, 0, 1
        );

        glm::vec3 zInversPosition = p_Model->position; //Inverse Z-Position of models.
        zInversPosition.z *= -1;

        //API-Info: One can do the Z-Inversion at fragment shader level, but that is not nessesarily faster, but definitly more work.

        ubo.model = glm::translate(glm::mat4(1.0f), zInversPosition) * glm::toMat4(p_Model->rotation) * glm::scale(zInversMatrix, p_Model->scale);
        glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.0f, -5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
        glm::mat4 ortho_projection = glm::ortho(0.0f, ((float)p_Model->p_Window->windowSize.width), 0.0f, -((float)p_Model->p_Window->windowSize.height), -550.0f, 550.0f);

        ubo.viewProjection = ortho_projection * view;

        void* rawData;

        #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
        VkResult vkResult = vkMapMemory(g_Application.device, p_Model->uniformBufferCase.memory, 0, sizeof(UniformBufferObject), 0, &rawData);
        ASSERT_VKRESULT_THROW("Could not map memory while updating a UI Model ubo buffer.", vkResult)
        #else
        vkMapMemory(g_Application.device, p_Model->uniformBufferCase.memory, 0, sizeof(UniformBufferObject), 0, &rawData);
        #endif

        memcpy(rawData, &ubo, sizeof(UniformBufferObject));
        vkUnmapMemory(g_Application.device, p_Model->uniformBufferCase.memory);

    }

    static void recreateWindow(Window* window) {

        vkDeviceWaitIdle(g_Application.device);

        int width; int height;
        glfwGetWindowSize(window->glfwWindow, &width, &height);
        window->windowSize = {(uint32_t) width, (uint32_t) height};
        if (width == 0 || height == 0) {
            return;
        }

        for (uint32_t i = 0; i < window->frames.size(); i++) {
            vkFreeCommandBuffers(g_Application.device, g_Application.commandPool, 1, &(window->frames[i].commandBuffer));
            vkDestroyFramebuffer(g_Application.device, window->frames[i].frameBuffer, nullptr);
            vkDestroyImageView(g_Application.device, window->frames[i].imageView, nullptr);
        }

        VkSwapchainKHR swapchain = window->swapchain;

        vkDestroyImageView(g_Application.device, window->colorImageView, nullptr);
        vkDestroyImage(g_Application.device, window->colorImage, nullptr);
        vkFreeMemory(g_Application.device, window->colorImageMemory, nullptr);

        vkDestroyImageView(g_Application.device, window->depthImageView, nullptr);
        vkDestroyImage(g_Application.device, window->depthImage, nullptr);
        vkFreeMemory(g_Application.device, window->depthImageMemory, nullptr);

        createColorBuffer(window->windowSize, window->colorImage, window->colorImageMemory, window->colorImageView);
        createDepthBuffer(window->windowSize, window->depthImage, window->depthImageMemory, window->depthImageView);

        createOrRecreateFrames(window->surface, window->swapchain, window->frames, window->colorImageView, window->depthImageView, window->windowSize);
        recordCommandBuffers(window);

        vkDestroySwapchainKHR(g_Application.device, swapchain, nullptr);
    }

    CreatoGraphicsResultInfo* drawframe(Window* p_Window) { //TODO ErrorHandling Make this critical and throw

        if (p_Window->windowSize.width == 0 || p_Window->windowSize.height == 0) {
            return LGRI_SUCCESS;
        }

        VkSemaphore &imageAquired = p_Window->syncSets[p_Window->syncIndex].imageAquiredSemaphore;
        VkSemaphore &renderingComplete = p_Window->syncSets[p_Window->syncIndex].renderingCompleteSemaphore;
        VkFence &inFlightFence = p_Window->syncSets[p_Window->syncIndex].inFlightFence;

        vkWaitForFences(g_Application.device, 1, &inFlightFence, VK_TRUE, UINT64_MAX);

        vkAcquireNextImageKHR(g_Application.device, p_Window->swapchain, std::numeric_limits<uint32_t>::max(), imageAquired, VK_NULL_HANDLE, &(p_Window->frameIndex));

        VkFence &frameFence = p_Window->frames[p_Window->frameIndex].frameFence;

        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (frameFence != VK_NULL_HANDLE) {
            vkWaitForFences(g_Application.device, 1, &frameFence, VK_TRUE, UINT64_MAX);
        }

        for (auto& model : updateModelList) {
            model->updateMPVFunc(model);
        }
        updateModelList.clear();

        if (p_Window->b_ReRecordCommandBuffers) {

            VkCommandBufferAllocateInfo commandBufferAllocateInfo;
            commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            commandBufferAllocateInfo.pNext = nullptr;
            commandBufferAllocateInfo.commandPool = g_Application.commandPool;
            commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            commandBufferAllocateInfo.commandBufferCount = 1;

            for (uint32_t i = 0; i < p_Window->frames.size(); i++) {
                vkResetCommandBuffer(p_Window->frames[i].commandBuffer, 0);
                VkResult vkResult = vkAllocateCommandBuffers(g_Application.device, &commandBufferAllocateInfo, &(p_Window->frames[i].commandBuffer));
                ASSERT_VKRESULT_THROW("Could not allocate VkCommandBuffers for swapchainImage.", vkResult);
            }
            recordCommandBuffers(p_Window);

            p_Window->b_ReRecordCommandBuffers = false;
        }

        // Mark the image as now being in use by this frame
        frameFence = inFlightFence;

        std::vector<VkPipelineStageFlags> pipelineStageFlags = {
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
        };

        VkSubmitInfo submitInfo;
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = nullptr;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &imageAquired;
        submitInfo.pWaitDstStageMask = pipelineStageFlags.data();
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &(p_Window->frames[p_Window->frameIndex].commandBuffer);
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &renderingComplete;

        vkResetFences(g_Application.device, 1, &inFlightFence);

        VkResult vkResult = vkQueueSubmit(g_Application.queue, 1, &submitInfo, inFlightFence);
        ASSERT_VKRESULT_LGR_T(vkResult, LGR_THROW_ERROR, "Could not submit Queue.")

        VkPresentInfoKHR presentInfo;
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.pNext = nullptr;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &renderingComplete;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &(p_Window->swapchain);
        presentInfo.pImageIndices = &(p_Window->frameIndex);
        presentInfo.pResults = nullptr;

        vkResult = vkQueuePresentKHR(g_Application.queue, &presentInfo);
        if (vkResult == VK_ERROR_OUT_OF_DATE_KHR || vkResult == VK_SUBOPTIMAL_KHR) {
            recreateWindow(p_Window);
        } else {
            ASSERT_VKRESULT_LGR_T(vkResult, LGR_THROW_ERROR, "Could not present queue.")
        }

        p_Window->syncIndex = (p_Window->syncIndex + 1) % g_Application.maxInFlightFrames;

        return LGRI_SUCCESS;
    }

    //TODO load pregenerated mipmaps from file if present, if format does not support linear filert, then use stb_image_resize fun or search for other formats
    static void generateMipmaps(VkImage image, VkFormat format, int32_t texWidth, int32_t texHeight, uint32_t mipLevels) {

        // Check if image format supports linear blitting
        VkFormatProperties formatProperties;
        vkGetPhysicalDeviceFormatProperties(g_Application.physicalDevice, format, &formatProperties);

        if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
            throw std::runtime_error("texture image format does not support linear blitting!");
        }

        VkCommandBuffer commandBuffer = beginOneTimeCommandBuffer(nullptr); //TODO nullptr argument, why use it then?

        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.image = image;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;
        barrier.subresourceRange.levelCount = 1;

        int32_t mipWidth = texWidth;
        int32_t mipHeight = texHeight;

        for (uint32_t i = 1; i < mipLevels; i++) {
            barrier.subresourceRange.baseMipLevel = i - 1;
            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

            VkImageBlit blit{};
            blit.srcOffsets[0] = {0, 0, 0};
            blit.srcOffsets[1] = {mipWidth, mipHeight, 1};
            blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.srcSubresource.mipLevel = i - 1;
            blit.srcSubresource.baseArrayLayer = 0;
            blit.srcSubresource.layerCount = 1;
            blit.dstOffsets[0] = {0, 0, 0};
            blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
            blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.dstSubresource.mipLevel = i;
            blit.dstSubresource.baseArrayLayer = 0;
            blit.dstSubresource.layerCount = 1;

            vkCmdBlitImage(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_LINEAR);

            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

            if (mipWidth > 1) mipWidth /= 2;
            if (mipHeight > 1) mipHeight /= 2;
        }

        barrier.subresourceRange.baseMipLevel = mipLevels - 1;
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        endOneTimeCommandBuffer(commandBuffer);
    }

    static CreatoGraphicsResultInfo* createBuffer(Buffer &bufferCase, VkBufferUsageFlags bufferUsageFlags, VkMemoryPropertyFlags memoryPropertyFlags) {
        VkBufferCreateInfo bufferCreateInfo;
        bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.pNext = nullptr;
        bufferCreateInfo.flags = 0;
        bufferCreateInfo.size = bufferCase.size;
        bufferCreateInfo.usage = bufferUsageFlags;
        bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferCreateInfo.queueFamilyIndexCount = 0;
        bufferCreateInfo.pQueueFamilyIndices = nullptr;

        VkResult vkResult = vkCreateBuffer(g_Application.device, &bufferCreateInfo, nullptr, &(bufferCase.buffer));
        ASSERT_VKRESULT_LGR(vkResult, LGR_CREATE_ERROR)

        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(g_Application.device, bufferCase.buffer, &memoryRequirements);

        VkMemoryAllocateInfo memoryAllocateInfo;
        memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        memoryAllocateInfo.pNext = nullptr;
        memoryAllocateInfo.allocationSize = memoryRequirements.size;
        memoryAllocateInfo.memoryTypeIndex = findMemoryTypeIndex(memoryRequirements.memoryTypeBits, memoryPropertyFlags);

        vkResult = vkAllocateMemory(g_Application.device, &memoryAllocateInfo, nullptr, &(bufferCase.memory));
        ASSERT_VKRESULT_LGR(vkResult, LGR_ALLOC_ERROR)

        vkResult = vkBindBufferMemory(g_Application.device, bufferCase.buffer, bufferCase.memory, 0);
        ASSERT_VKRESULT_LGR(vkResult, LGR_BIND_ERROR)

        return LGRI_SUCCESS;
    }

    static Texture* createTexture(const unsigned char* pixels, glm::vec2 size, uint32_t mipLevels, VkFormat format, VkImageTiling tiling, bool b_IsMultiModelTexture) {

        Buffer stagingBufferCase;
        stagingBufferCase.size = size.x * size.y * 4;
        createBuffer(stagingBufferCase, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

        void* data;
        vkMapMemory(g_Application.device, stagingBufferCase.memory, 0, stagingBufferCase.size, 0, &data);
        memcpy(data, pixels, static_cast<size_t>(stagingBufferCase.size));
        vkUnmapMemory(g_Application.device, stagingBufferCase.memory);

        Texture* p_Texture = new Texture();
        p_Texture->size = size;

        p_Texture->b_IsMultiModelTexture = b_IsMultiModelTexture;
        p_Texture->mipLevels = mipLevels;

        createImage(size.x, size.y, p_Texture->mipLevels, VK_SAMPLE_COUNT_1_BIT, format, tiling, VK_IMAGE_USAGE_TRANSFER_SRC_BIT |VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, p_Texture->image, p_Texture->imageMemory);

        transitionImageLayout(p_Texture->image, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, p_Texture->mipLevels);
        { // Copy Buffer into Image
            VkCommandBuffer commandBuffer = beginOneTimeCommandBuffer(nullptr); //TODO

            VkBufferImageCopy region;
            region.bufferOffset = 0;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;
            region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.mipLevel = 0;
            region.imageSubresource.baseArrayLayer = 0;
            region.imageSubresource.layerCount = 1;
            region.imageOffset = {0, 0, 0};
            region.imageExtent = {static_cast<uint32_t>(size.x), static_cast<size_t>(size.y), 1};

            vkCmdCopyBufferToImage(commandBuffer, stagingBufferCase.buffer, p_Texture->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

            endOneTimeCommandBuffer(commandBuffer);
        }

        vkDestroyBuffer(g_Application.device, stagingBufferCase.buffer, nullptr);
        vkFreeMemory(g_Application.device, stagingBufferCase.memory, nullptr);

        //transitionImageLayout(Texture->image, format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, Texture->mipLevels);
        generateMipmaps(p_Texture->image, format, size.x, size.y, p_Texture->mipLevels);

        CreatoGraphicsResultInfo* lrgi = createImageView(p_Texture->image, p_Texture->imageView, format, VK_IMAGE_ASPECT_COLOR_BIT, p_Texture->mipLevels);
        if (lrgi) {
            ASSERT_VKRESULT_THROW("Could not create ImageView for Texture!", lrgi->vkResult); //TODO refire lrgi?
        }

        { // Create Sampler //TODO use sampler for multiple images
            VkSamplerCreateInfo samplerCreateInfo;
            samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
            samplerCreateInfo.pNext = nullptr;
            samplerCreateInfo.flags = 0;
            samplerCreateInfo.magFilter = VK_FILTER_NEAREST;
            samplerCreateInfo.minFilter = VK_FILTER_NEAREST;
            samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerCreateInfo.mipLodBias = 0.0f;
            samplerCreateInfo.anisotropyEnable = VK_TRUE;
            VkPhysicalDeviceProperties properties{};
            vkGetPhysicalDeviceProperties(g_Application.physicalDevice, &properties);
            samplerCreateInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
            samplerCreateInfo.compareEnable = VK_FALSE;
            samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
            samplerCreateInfo.minLod = 0.0f;
            samplerCreateInfo.maxLod = static_cast<float>(p_Texture->mipLevels);
            samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
            samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;

            if (vkCreateSampler(g_Application.device, &samplerCreateInfo, nullptr, &(p_Texture->sampler)) != VK_SUCCESS) {
                __debugbreak();
                //TODO error message
                return nullptr;
            }
        }

        return p_Texture;
    }

    Texture* createTexture(std::string path, bool b_IsMultiModelTexture) {

        if (!_filesystem::is_regular_file(_filesystem::path(path))) {
            throw std::runtime_error("Parameter path is not a path to a regular file!");
        }

        if ((strcmp(_filesystem::extension(path).c_str(), ".png") && strcmp(_filesystem::extension(path).c_str(), ".jpg"))) {
            throw std::runtime_error("Not a supported file format!");
        }

        int height, width, channels = 4;
        stbi_uc* pixels = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);

        if (!pixels) {
            throw std::runtime_error("Could not load image into memory!");
        }

        uint32_t mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;
        Texture* p_Texture = createTexture(pixels, glm::vec2(width, height), mipLevels, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, b_IsMultiModelTexture);

        stbi_image_free(pixels);

        return p_Texture;
    }

    Texture* createSingleColorTexture(const RGBA8 &hexColor, bool b_IsMultiModelTexture) {
        return createTexture(hexColor.data(), glm::vec2(1, 1), 1, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, b_IsMultiModelTexture);
    }

    Texture* createMultiColorTexture(const RGBA8_M& hexColor, bool b_IsMultiModelTexture) {
        return createTexture(hexColor.data(), glm::vec2(2, 1), 1, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, b_IsMultiModelTexture);
    }

    static void createFontSDF(const std::string& fontFileName) {

        std::string path = filesystem::makeAbsoluteProcPath("/fonts/" + fontFileName);

        if (!_filesystem::is_regular_file(_filesystem::path(path))) {
            throw std::runtime_error("Parameter path is not a path to a regular file!");
        }

        if (strcmp(_filesystem::extension(path).c_str(), ".ttf")) {
            throw std::runtime_error("Not a .ttf file format!");
        }

        stbtt_fontinfo font;

        RGBA8_M fileBuffer = readFileUnsigned(path.c_str());
        stbtt_InitFont(&font, fileBuffer.data(), stbtt_GetFontOffsetForIndex(fileBuffer.data(), 0));

        float fontSize = 42, padding = 8;
        float scale = stbtt_ScaleForPixelHeight(&font, fontSize);
        FontBitmap* p_FB = new FontBitmap();
        fontSDFTextures[fontFileName] = p_FB;
        uint32_t textureHeight = 0;

        for(uint32_t i = 33; i < 128; i++) {

            int posX, posY, width, height, ascent, descent, linegap;

            unsigned char* bitmap = stbtt_GetCodepointSDF(&font, scale, (char)i, padding, 128, 5, &width, &height, &posX, &posY);
            stbtt_GetFontVMetrics(&font, &ascent, &descent, &linegap);

            RGBA8_M bits(bitmap, bitmap + (width)*(height));

            CharacterBitmap* p_CB = new CharacterBitmap(0, {posX, (int)(posY + scale * ascent)}, {width, height}, bits);

            for (int q = width - 1; q >= 0; q--) {
                for (int p = 0; p < height; p++) {
                    p_FB->pixels.push_back(bitmap[q + p * width]);
                    p_FB->pixels.push_back(bitmap[q + p * width]);
                    p_FB->pixels.push_back(bitmap[q + p * width]);
                    p_FB->pixels.push_back(bitmap[q + p * width]);
                }
                for (int p = 0; p < (fontSize + 2 * padding - height); p++) {
                    p_FB->pixels.push_back(0);
                    p_FB->pixels.push_back(0);
                    p_FB->pixels.push_back(0);
                    p_FB->pixels.push_back(0);
                }
            }

            characterSDFs[fontFileName][i] = p_CB;
            p_CB->texPos = textureHeight;
            free(bitmap);
            textureHeight += width;
        }

        uint32_t mipLevels = 1;
        p_FB->p_Texture = createTexture(p_FB->pixels.data(), glm::vec2(fontSize + 2*padding, textureHeight), mipLevels, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, true);
    }

    std::vector<Vertex> getCharacterVertices(std::string fontFileName, unsigned long codepoint, glm::vec2& pos, graphics::Texture** p_Texture) {

        if (fontSDFTextures.empty() || fontSDFTextures.find(fontFileName) == fontSDFTextures.end()) {
            createFontSDF(fontFileName);
        }

        CharacterBitmap* cb = characterSDFs[fontFileName][codepoint];
        FontBitmap* fb = fontSDFTextures.at(fontFileName);
        glm::ivec2& size = fb->p_Texture->size;

        std::vector<Vertex> vertices = {
            Vertex({pos.x + cb->pos.x             , pos.y + cb->pos.y             , 0.0f}, {                           0.0f, ((float)(cb->texPos + cb->size.x)) / size.y}),
            Vertex({pos.x + cb->pos.x + cb->size.x, pos.y + cb->pos.y             , 0.0f}, {                           0.0f, ((float)(cb->texPos))              / size.y}),
            Vertex({pos.x + cb->pos.x + cb->size.x, pos.y + cb->pos.y + cb->size.y, 0.0f}, { ((float)(cb->size.y)) / size.x, ((float)(cb->texPos))              / size.y}),
            Vertex({pos.x + cb->pos.x             , pos.y + cb->pos.y + cb->size.y, 0.0f}, { ((float)(cb->size.y)) / size.x, ((float)(cb->texPos + cb->size.x)) / size.y})
        };

        *p_Texture = fb->p_Texture;
        pos.x += cb->size.x - 8; //TODO also vertically

        return vertices;
    }

    /**
     * @param fontFilePath can be absolut, or relative to the directory of the executable.
     */
    static void createFontBitmap(const std::string& fontFilePath, uint32_t fontSize) {

        std::string path = filesystem::makeAbsoluteProcPath("/fonts/" + fontFilePath);

        if (!_filesystem::is_regular_file(_filesystem::path(path))) {
            throw std::runtime_error("Parameter path is not a path to a regular file!");
        }

        if (strcmp(_filesystem::extension(path).c_str(), ".ttf")) {
            throw std::runtime_error("Not a .ttf file format!");
        }

        stbtt_fontinfo font;

        RGBA8_M fileBuffer = readFileUnsigned(path.c_str());

        stbtt_InitFont(&font, fileBuffer.data(), stbtt_GetFontOffsetForIndex(fileBuffer.data(), 0));
        float scale = stbtt_ScaleForPixelHeight(&font, (float)fontSize);
        for(uint32_t i = 32; i < 128; i++) { //TODO all present characters pls
            int posX, posY, width, height;
            unsigned char* bitmap = stbtt_GetCodepointBitmap(&font, 0, scale, (char)i, &width, &height, &posX, &posY);
            int ascent, descent, linegap;
            stbtt_GetFontVMetrics(&font, &ascent, &descent, &linegap);
            RGBA8_M bits(bitmap, bitmap + width*height);
            CharacterBitmap* p_CB = new CharacterBitmap(0, {posX, (int)(posY + scale * ascent)}, {width, height}, bits);
            characterBitMaps[path][fontSize][i] = p_CB;
            free(bitmap);
        }
    }

    void createFontBitmaps(const std::vector<std::pair<std::string, uint32_t>>& fonts) {
        for (const auto& [font, size] : fonts) {
            createFontBitmap(font, size);
        }
    }

    void createFontSDFs(const std::vector<std::string>& fonts) {
        for (const auto& font : fonts) {
            createFontSDF(font);
        }
    }

    //Does not free the reserved space for the texture pointer itself, use "delete texture" for that, which conveniently calls this func for you.
    void destroyTexture(Texture* p_Texture) {
        vkDestroySampler(g_Application.device, p_Texture->sampler, nullptr);
        vkDestroyImageView(g_Application.device, p_Texture->imageView, nullptr);
        vkDestroyImage(g_Application.device, p_Texture->image, nullptr);
        vkFreeMemory(g_Application.device, p_Texture->imageMemory, nullptr);
    }

    static CreatoGraphicsResultInfo* createDescriptorPool(VkDescriptorPool &descriptorPool) {

        std::array<VkDescriptorPoolSize, 2> poolSizes{};
        poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        poolSizes[0].descriptorCount = 1; //TODO: static_cast<uint32_t>(swapChainImages.size());
        poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        poolSizes[1].descriptorCount = 1; //TODO: static_cast<uint32_t>(swapChainImages.size());

        VkDescriptorPoolCreateInfo descriptorPoolCreateInfo;
        descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolCreateInfo.pNext = nullptr;
        descriptorPoolCreateInfo.flags = 0;
        descriptorPoolCreateInfo.maxSets = 2;
        descriptorPoolCreateInfo.poolSizeCount = poolSizes.size();
        descriptorPoolCreateInfo.pPoolSizes = poolSizes.data();

        VkResult vkResult = vkCreateDescriptorPool(g_Application.device, &descriptorPoolCreateInfo, nullptr, &(descriptorPool));
        ASSERT_VKRESULT_LGR(vkResult, LGR_ERROR)

        return LGRI_SUCCESS;
    }

    static CreatoGraphicsResultInfo* createDescriptorSets(VkDescriptorPool &descriptorPool, VkDescriptorSet &descriptorSet, Buffer &uniformBufferCase, VkImageView &textureImageView, VkSampler &textureSampler) {

        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo;
        descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        descriptorSetAllocateInfo.pNext = nullptr;
        descriptorSetAllocateInfo.descriptorPool = descriptorPool;
        descriptorSetAllocateInfo.descriptorSetCount = 1;
        descriptorSetAllocateInfo.pSetLayouts = &(g_Application.descriptorSetLayout);

        VkResult vkResult = vkAllocateDescriptorSets(g_Application.device, &descriptorSetAllocateInfo, &descriptorSet);
        ASSERT_VKRESULT_LGR(vkResult, LGR_ERROR)

        VkDescriptorBufferInfo descriptorBufferInfo;
        descriptorBufferInfo.buffer = uniformBufferCase.buffer;
        descriptorBufferInfo.offset = 0;
        descriptorBufferInfo.range = uniformBufferCase.size;

        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = textureImageView;
        imageInfo.sampler = textureSampler;

        std::array<VkWriteDescriptorSet, 2> writeDescriptorSets{};

        writeDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSets[0].pNext = nullptr;
        writeDescriptorSets[0].dstSet = descriptorSet;
        writeDescriptorSets[0].dstBinding = 0;
        writeDescriptorSets[0].dstArrayElement = 0;
        writeDescriptorSets[0].descriptorCount = 1;
        writeDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writeDescriptorSets[0].pImageInfo = nullptr;
        writeDescriptorSets[0].pBufferInfo = &descriptorBufferInfo;
        writeDescriptorSets[0].pTexelBufferView = nullptr;
        
        writeDescriptorSets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSets[1].pNext = nullptr;
        writeDescriptorSets[1].dstSet = descriptorSet;
        writeDescriptorSets[1].dstBinding = 1;
        writeDescriptorSets[1].dstArrayElement = 0;
        writeDescriptorSets[1].descriptorCount = 1;
        writeDescriptorSets[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writeDescriptorSets[1].pImageInfo = nullptr;
        writeDescriptorSets[1].pImageInfo = &imageInfo;
        writeDescriptorSets[1].pTexelBufferView = nullptr;
        
        vkUpdateDescriptorSets(g_Application.device, static_cast<uint32_t>(writeDescriptorSets.size()), writeDescriptorSets.data(), 0, nullptr);

        return LGRI_SUCCESS;
    }

    static void copyBuffer(VkBuffer &src, VkBuffer &dst, VkDeviceSize &size) { //TODO ErrorHandling
        VkCommandBuffer commandBuffer = beginOneTimeCommandBuffer(nullptr); //TODO

        VkBufferCopy bufferCopy;
        bufferCopy.srcOffset = 0;
        bufferCopy.dstOffset = 0;
        bufferCopy.size = size;

        vkCmdCopyBuffer(commandBuffer, src, dst, 1, &bufferCopy);

        endOneTimeCommandBuffer(commandBuffer);
    }

    template <typename T>
    static void createStagedBuffer(Buffer& bufferCase, VkBufferUsageFlags usage, std::vector<T>& bufferData) { //TODO ErrorHandling
        bufferCase.size = sizeof(T) * bufferData.size();

        Buffer stagingBufferCase;
        stagingBufferCase.size = bufferCase.size;

        VkBufferUsageFlags bufferFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        VkMemoryPropertyFlags memoryFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        createBuffer(stagingBufferCase, bufferFlags, memoryFlags);

        void* rawData;

        #ifdef CREATO_UTILS_GRAPHICS_DEBUG_BUILD
        VkResult vkResult = vkMapMemory(g_Application.device, stagingBufferCase.memory, 0, bufferCase.size, 0, &rawData);
        ASSERT_VKRESULT_THROW("Could not map memory while creating a staged buffer.", vkResult)
        #else
        vkMapMemory(g_Application.device, stagingBufferCase.memory, 0, bufferCase.size, 0, &rawData);
        #endif

        memcpy(rawData, bufferData.data(), bufferCase.size);
        vkUnmapMemory(g_Application.device, stagingBufferCase.memory);
        //vkFlushMappedMemoryRanges (faster)

        createBuffer(bufferCase, usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        copyBuffer(stagingBufferCase.buffer, bufferCase.buffer, bufferCase.size);

        vkDestroyBuffer(g_Application.device, stagingBufferCase.buffer, nullptr);
        vkFreeMemory(g_Application.device, stagingBufferCase.memory, nullptr);
    }

    static void createUniformBuffer(Buffer& uniformBufferCase) { //TODO Errorhandling
        uniformBufferCase.size = sizeof(UniformBufferObject);
        VkMemoryPropertyFlags memoryPropertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        createBuffer(uniformBufferCase, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, memoryPropertyFlags);
    }

    void createVertexBuffer(Buffer& vertexBufferCase, std::vector<Vertex>& verticies) { //TODO Errorhandling //TODO static
        createStagedBuffer<Vertex>(vertexBufferCase, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, verticies);
    }

    void createIndexBuffer(Buffer &uniformBufferCase, std::vector<uint32_t> &indices) { //TODO errorhandling //TODO static
        return createStagedBuffer(uniformBufferCase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, indices);
    }

    Mesh* createMesh(std::string path, bool b_IsMultiModelMesh) {

        if (!_filesystem::is_regular_file(_filesystem::path(path))) {
            //TODO error message
            return nullptr;
        }

        if (strcmp(_filesystem::extension(path).c_str(), ".obj")) {
            //TODO error message
            return nullptr;
        }

        tinyobj::attrib_t attrib;
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;
        std::string warn, err;

        if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, path.c_str())) {
            throw std::runtime_error(warn + err);
        }

        //TODO print warn and error msg anyway if existant

        Mesh* mesh = new Mesh();

        mesh->b_IsMultiModelMesh = b_IsMultiModelMesh;

        std::unordered_map<Vertex, uint32_t> uniqueVertices{};

        for (const auto& shape : shapes) {
            for (const auto& index : shape.mesh.indices) {

                Vertex vertex;

                vertex.pos = {
                    attrib.vertices[3 * index.vertex_index + 0],
                    attrib.vertices[3 * index.vertex_index + 1],
                    attrib.vertices[3 * index.vertex_index + 2]
                };

                vertex.texCoord = {
                    attrib.texcoords[2 * index.texcoord_index + 0],
                    1.0f - attrib.texcoords[2 * index.texcoord_index + 1] //TODO: make param
                };

                if (uniqueVertices.count(vertex) == 0) {
                    uniqueVertices[vertex] = static_cast<uint32_t>(mesh->v_VertexList.size());
                    mesh->v_VertexList.push_back(vertex);
                }

                mesh->v_IndexList.push_back(uniqueVertices[vertex]);
            }
        }

        createVertexBuffer(mesh->vertexBufferCase, mesh->v_VertexList);
        createIndexBuffer(mesh->indexBufferCase, mesh->v_IndexList);

        return mesh;
    }
    
    void destroyMesh(Mesh* p_Mesh) {

        vkFreeMemory(g_Application.device, p_Mesh->indexBufferCase.memory, nullptr);
        vkDestroyBuffer(g_Application.device, p_Mesh->indexBufferCase.buffer, nullptr);

        vkFreeMemory(g_Application.device, p_Mesh->vertexBufferCase.memory, nullptr);
        vkDestroyBuffer(g_Application.device, p_Mesh->vertexBufferCase.buffer, nullptr);
    }

    Model* createModel(Mesh* p_Mesh, Texture* p_Texture, Window* p_Window) {

        Model* p_Model = new Model();

        p_Model->p_Window = p_Window;
        p_Model->p_Texture = p_Texture;
        p_Model->p_Mesh = p_Mesh;
        p_Model->updateMPVFunc = updateModelViewProjection;

        createUniformBuffer(p_Model->uniformBufferCase);
        createDescriptorPool(p_Model->descriptorPool);
        createDescriptorSets(p_Model->descriptorPool, p_Model->descriptorSet, p_Model->uniformBufferCase, p_Texture->imageView, p_Texture->sampler);

        p_Window->sceneRenderList.insert(p_Model);
        p_Window->b_ReRecordCommandBuffers = true;

        updateModel(p_Model);

        return p_Model;
    }

    Model* createInterfaceModel(Mesh* p_Mesh, Texture* p_Texture, Window* p_Window) {

        Model* p_Model = new Model();

        p_Model->p_Window = p_Window;
        p_Model->p_Texture = p_Texture;
        p_Model->p_Mesh = p_Mesh;
        p_Model->updateMPVFunc = updateModelViewUIProjection;

        createUniformBuffer(p_Model->uniformBufferCase);
        createDescriptorPool(p_Model->descriptorPool);
        createDescriptorSets(p_Model->descriptorPool, p_Model->descriptorSet, p_Model->uniformBufferCase, p_Texture->imageView, p_Texture->sampler);

        p_Window->transparentInterfaceRenderList.insert(p_Model);
        p_Window->b_ReRecordCommandBuffers = true;

        graphics::updateModel(p_Model);

        return p_Model;
    }

    Model* createInterfaceModel(float posX, float posY, float posZ, Texture* p_Texture, Window* p_Window, bool opaque) { //TODO this might be outdated

        Model* p_Model = new Model();

        p_Model->p_Window = p_Window;
        p_Model->p_Texture = p_Texture;

        p_Model->p_Mesh = new graphics::Mesh();
        p_Model->p_Mesh->v_VertexList = {
            Vertex({ posX,                     posY,                     posZ}, {0.0f, 0.0f}),
            Vertex({ posX,                     posY + p_Texture->size.y, posZ}, {0.0f, 1.0f}),
            Vertex({ posX + p_Texture->size.x, posY,                     posZ}, {1.0f, 0.0f}),
            Vertex({ posX + p_Texture->size.x, posY + p_Texture->size.y, posZ}, {1.0f, 1.0f})
        };
        p_Model->p_Mesh->v_IndexList = {0, 1, 3, 3, 2, 0};
        createVertexBuffer(p_Model->p_Mesh->vertexBufferCase, p_Model->p_Mesh->v_VertexList);
        createIndexBuffer(p_Model->p_Mesh->indexBufferCase, p_Model->p_Mesh->v_IndexList);

        createUniformBuffer(p_Model->uniformBufferCase);
        createDescriptorPool(p_Model->descriptorPool);
        createDescriptorSets(p_Model->descriptorPool, p_Model->descriptorSet, p_Model->uniformBufferCase, p_Texture->imageView, p_Texture->sampler);

        if (opaque) {
            p_Window->opaqueInterfaceRenderList.insert(p_Model);
        } else {
            p_Window->transparentInterfaceRenderList.insert(p_Model);
        }

        p_Window->b_ReRecordCommandBuffers = true;

        return p_Model;
    }

    Model* createModel(std::string path, Texture* p_Texture, Window* p_Window) {
        Mesh* p_Mesh = createMesh(path, false);
        if (!p_Mesh) {
            //TODO error message
            return nullptr;
        }

        return createModel(p_Mesh, p_Texture, p_Window);
    }

    Model* createModel(std::string meshPath, std::string texturePath, Window* p_Window) {

        Texture* p_Texture = createTexture(texturePath, false);
        if (!p_Texture) {
            //TODO error message
            return nullptr;
        }

        return createModel(meshPath, p_Texture, p_Window);
    }

    void updateModel(Model* p_Model) {
        updateModelList.insert(p_Model);
    }

    void destroyModel(Model* p_Model) {

        eraseModelFromRenderLists(p_Model);

        vkDestroyDescriptorPool(g_Application.device, p_Model->descriptorPool, nullptr);
        vkFreeMemory(g_Application.device, p_Model->uniformBufferCase.memory, nullptr);
        vkDestroyBuffer(g_Application.device, p_Model->uniformBufferCase.buffer, nullptr);

        if (!(p_Model->p_Mesh->b_IsMultiModelMesh)) {
            delete p_Model->p_Mesh;
        }

        if (!(p_Model->p_Texture->b_IsMultiModelTexture)) {
            delete p_Model->p_Texture;
        }
    }

    void shutdown() {

        vkDeviceWaitIdle(g_Application.device);

        vkDestroyDescriptorSetLayout(g_Application.device, g_Application.descriptorSetLayout, nullptr);

        vkDestroyCommandPool(g_Application.device, g_Application.commandPool, nullptr);

        vkDestroyPipeline(g_Application.device, g_Application.scenePipeline, nullptr);
        vkDestroyRenderPass(g_Application.device, g_Application.renderPass, nullptr);
        vkDestroyPipelineLayout(g_Application.device, g_Application.pipelineLayout, nullptr);

        vkDestroyDevice(g_Application.device, nullptr);
        vkDestroyInstance(g_Application.instance, nullptr);

        glfwTerminate();

    }

} // namespace creato::graphics