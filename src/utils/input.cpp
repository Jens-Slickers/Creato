/**
 * 
 */

//Attached Header
#include <utils/input.hpp>

//STD
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <map>

//In-House
#include <utils/defines.hpp>

//#define CREATO_UTILS_INPUT_DEBUG_BUILD

#ifdef CREATO_GLOBAL_DEBUG_BUILD
#define CREATO_UTILS_INPUT_DEBUG_BUILD
#endif

#ifdef CREATO_UTILS_INPUT_DEBUG_BUILD
#include <iostream>
#endif

namespace creato::input {

    std::unordered_map<GLFWwindow*, std::unordered_set<CursorMoveCallback>> cursorMoveCallbacks;

    std::unordered_map<GLFWwindow*, std::unordered_map<Shortcut, std::unordered_set<ShortcutCallback>>> shortcutCallbacks;

    std::unordered_map<GLFWwindow*, std::unordered_set< Area*>> areaLists;
    std::unordered_map<Shortcut, std::multimap<int, Area*>> currentAreaMap;

    std::unordered_map<GLFWwindow*, std::unordered_set<CharacterCallback>> characterCallbacks;

    static void calculateInputAreasAtPos(GLFWwindow* p_Window, double xpos, double ypos) {
        currentAreaMap.clear();
        for (Area* p_Area : areaLists[p_Window]) {
            if (p_Area->b_IsHovered) {
                if (!p_Area->containsPoint(xpos, ypos)) {
                    p_Area->b_IsHovered = false;
                    p_Area->m_LeaveCallback();
                }
            } else {
                if (p_Area->containsPoint(xpos, ypos)) {
                    currentAreaMap[p_Area->m_Shortcut].insert({p_Area->m_PosZ, p_Area});
                    p_Area->b_IsHovered = true;
                    p_Area->m_EnterCallback();
                }
            }
        }
    }

    void registerCursorMoveCallback(GLFWwindow* p_Window, const CursorMoveCallback callback) {
        cursorMoveCallbacks[p_Window].insert(callback);
    }

    void eraseCursorMoveCallback(GLFWwindow* p_Window, const CursorMoveCallback callback) {
        cursorMoveCallbacks[p_Window].erase(callback);
    }

    void eraseAllCursorMoveCallbacks(GLFWwindow* p_Window) {
        for (const CursorMoveCallback& it : cursorMoveCallbacks[p_Window]) {
            eraseCursorMoveCallback(p_Window, it);
        }
    }

    static void cursorMoveCallback(GLFWwindow* p_Window, double xpos, double ypos) {

        //Add all InputAreas, which the cursor is currently in, to the currentInputAreaMap list.
        calculateInputAreasAtPos(p_Window, xpos, ypos);

        //call cursorMoveCallbacks (only for relevant window)
        for (const CursorMoveCallback& callback : cursorMoveCallbacks[p_Window]) {
            callback(xpos, ypos);
        }
    }

    void registerInputCallback(GLFWwindow* p_Window, Shortcut shortcut, ShortcutCallback callback) {
        shortcutCallbacks[p_Window][shortcut].insert(callback);
    }

    void eraseInputCallback(GLFWwindow* p_Window, Shortcut shortcut, ShortcutCallback callback) {
        shortcutCallbacks[p_Window][shortcut].erase(callback);
    }

    void eraseAllInputCallbacks(GLFWwindow* p_Window, ShortcutCallback callback) {
        for (auto& it : shortcutCallbacks[p_Window]) {
            eraseInputCallback(p_Window, it.first, callback);
        }
    }

    void buttonCallback(GLFWwindow* p_Window, int button, int action, int mods) {
        Shortcut shortcut = button + (mods << MOD_BITSHIFT) + (1 << 30);

        #ifdef CREATO_UTILS_INPUT_DEBUG_BUILD
        std::cout << "Mousebutton Input: " << "button: " << button << ", " << ", action: " << action << ", mods: " << mods << ", shortcut: " << shortcut << std::endl;
        #endif

        for (const ShortcutCallback& callback : shortcutCallbacks[p_Window][shortcut]) {
            callback(action, shortcut);
        }

        for (auto& [posZ, p_Area] : currentAreaMap[shortcut]) { //TODO Inverse List?
            
            p_Area->m_InputCallback();

            if (p_Area->mb_ConsumeInput) {
                break;
            }
        }
    }

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-parameter"

    /**
     * @param p_Window
     * @param key is a token for each keyboard key. Some rare or custom keyboardkey may have no tokens, then this is GLFW_KEY_UNKNOWN.
     * @param scancode is a unique idetifier for each key, that is platform-specific, but persistant over time.
     * @param action is one of GLFW_PRESS, GLFW_REPEAT, od GLFW_RELEASE.
     */
    void keyCallback(GLFWwindow* p_Window, int key, int scancode, int action, int mods) {
        Shortcut shortcut = scancode + (mods << MOD_BITSHIFT);

        #ifdef CREATO_UTILS_INPUT_DEBUG_BUILD
        std::cout << "Keyboard Input: " << "key: " << key << ", scancode: " << scancode << ", action: " << action << ", mods: " << mods << ", shortcut: " << shortcut << std::endl;
        #endif

        for (const ShortcutCallback& callback : shortcutCallbacks[p_Window][shortcut]) {
            callback(action, shortcut);
        }

        for (auto& [posZ, p_Area] : currentAreaMap[shortcut]) { //TODO Inverse List?
            
            p_Area->m_InputCallback();

            if (p_Area->mb_ConsumeInput) {
                break;
            }
        }
    }

    #pragma GCC diagnostic pop

    void registerCharacterCallback(GLFWwindow* p_Window, const CharacterCallback callback) {
        characterCallbacks[p_Window].insert(callback);
    }

    void eraseCharacterCallback(GLFWwindow* p_Window, const CharacterCallback callback) {
        characterCallbacks[p_Window].erase(callback);
    }

    void eraseAllCharacterCallbacks(GLFWwindow* p_Window) {
        for (const CharacterCallback& callback : characterCallbacks[p_Window]) {
            eraseCharacterCallback(p_Window, callback);
        }
    }

    static void characterCallback(GLFWwindow* p_Window, UnicodeCodePoint codepoint) {

        #ifdef CREATO_UTILS_INPUT_DEBUG_BUILD
        std::cout << "character: " << codepoint << std::endl;
        #endif

        for (const CharacterCallback& callback : characterCallbacks[p_Window]) {
            callback(codepoint);
        }
    }

    void initWindowInput(GLFWwindow* p_Window) {
        glfwSetCharCallback(p_Window, characterCallback);
        glfwSetKeyCallback(p_Window, keyCallback);
        glfwSetMouseButtonCallback(p_Window, buttonCallback);
        glfwSetCursorPosCallback(p_Window, cursorMoveCallback);
    }

    void init() {

    }

    void shutdown() {
        
    }

} // namespace creato::input