/**
 * 
 */

#pragma once

#include <boost/filesystem.hpp>
#include <boost/dll/runtime_symbol_info.hpp>

namespace _filesystem = ::boost::filesystem;

namespace creato::filesystem {

    std::string getProcessDirectory();

    std::string makeAbsoluteProcPath(std::string relativePath);

} // namespace creato::filesystem
