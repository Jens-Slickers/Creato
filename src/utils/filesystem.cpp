/**
 * 
 */

//Attached Header
#include <utils/filesystem.hpp>

//In-House
#include <utils/defines.hpp>

namespace creato::filesystem {
    
    std::string getProcessDirectory() {
        return ::boost::dll::program_location().remove_filename().string();
    }

    /**
     *  Returns a new string, that is an absolute Path, leading through the exe directory to your relative path
     */
    std::string makeAbsoluteProcPath(std::string path) {
        if (_filesystem::path(path).is_relative()) {
            return getProcessDirectory() + "/" + path;
        } else {
            return path;
        }
    }

} // namespace creato::filesystem
