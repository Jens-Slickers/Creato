/** This module handels user input from keyboard and mouse.
 *
 * The name "button" always referes to a mouse button, and the name "key" always referes to a keyboard key.
 * This module can handle 8 mouse buttons (see glfw input guide).
 * Keys always are platform-specific scancodes, not the glfw enum.
 */

#pragma once

//In-House
#include <utils/glfw_vulkan.hpp>

namespace creato::input {

    /** segmented, xx yyyyyy zzz...
     *  xx : 00 = Keyboard, 01 = Mouse, 10 = Gamepad, 11 = future use
     *  yyyyyy : (MSB - LSB) NUM_LOCK, CAPS_LOCK, SUPER, ALT, CTRL, SHIFT.
     * zzz... the GLFW index of the input.
     */
    typedef uint32_t Shortcut;

    #pragma region  /* ===== Input TScancodes ===== */

    const Shortcut UNKNOWN = 0;

    /* ===== Printable keys ===== */

    const Shortcut KEYBOARD_DEVICE = (0 << 30);
                                            
    const Shortcut KEY_1                 = GLFW_KEY_1;
    const Shortcut KEY_2                 = GLFW_KEY_2;
    const Shortcut KEY_3                 = GLFW_KEY_3;
    const Shortcut KEY_4                 = GLFW_KEY_4;
    const Shortcut KEY_5                 = GLFW_KEY_5;
    const Shortcut KEY_6                 = GLFW_KEY_6;
    const Shortcut KEY_7                 = GLFW_KEY_7;
    const Shortcut KEY_8                 = GLFW_KEY_8;
    const Shortcut KEY_9                 = GLFW_KEY_9;
    const Shortcut KEY_0                 = GLFW_KEY_0;
    const Shortcut KEY_A                 = GLFW_KEY_A;
    const Shortcut KEY_B                 = GLFW_KEY_B;
    const Shortcut KEY_C                 = GLFW_KEY_C;
    const Shortcut KEY_D                 = GLFW_KEY_D;
    const Shortcut KEY_E                 = GLFW_KEY_E;
    const Shortcut KEY_F                 = GLFW_KEY_F;
    const Shortcut KEY_G                 = GLFW_KEY_G;
    const Shortcut KEY_H                 = GLFW_KEY_H;
    const Shortcut KEY_I                 = GLFW_KEY_I;
    const Shortcut KEY_J                 = GLFW_KEY_J;
    const Shortcut KEY_K                 = GLFW_KEY_K;
    const Shortcut KEY_L                 = GLFW_KEY_L;
    const Shortcut KEY_M                 = GLFW_KEY_M;
    const Shortcut KEY_N                 = GLFW_KEY_N;
    const Shortcut KEY_O                 = GLFW_KEY_O;
    const Shortcut KEY_P                 = GLFW_KEY_P;
    const Shortcut KEY_Q                 = GLFW_KEY_Q;
    const Shortcut KEY_R                 = GLFW_KEY_R;
    const Shortcut KEY_S                 = GLFW_KEY_S;
    const Shortcut KEY_T                 = GLFW_KEY_T;
    const Shortcut KEY_U                 = GLFW_KEY_U;
    const Shortcut KEY_V                 = GLFW_KEY_V;
    const Shortcut KEY_W                 = GLFW_KEY_W;
    const Shortcut KEY_X                 = GLFW_KEY_X;
    const Shortcut KEY_Y                 = GLFW_KEY_Y;
    const Shortcut KEY_Z                 = GLFW_KEY_Z;
    const Shortcut KEY_SPACE             = GLFW_KEY_SPACE;              /* Standard US-Layout of Special Characters below: */
    const Shortcut KEY_SPECIAL_0         = GLFW_KEY_APOSTROPHE;         /* ' */
    const Shortcut KEY_SPECIAL_1         = GLFW_KEY_COMMA;              /* , */
    const Shortcut KEY_SPECIAL_2         = GLFW_KEY_MINUS;              /* - */
    const Shortcut KEY_SPECIAL_3         = GLFW_KEY_PERIOD;             /* . */
    const Shortcut KEY_SPECIAL_4         = GLFW_KEY_SLASH;              /* / */
    const Shortcut KEY_SPECIAL_5         = GLFW_KEY_SEMICOLON;          /* ; */
    const Shortcut KEY_SPECIAL_6         = GLFW_KEY_EQUAL;              /* = */
    const Shortcut KEY_SPECIAL_7         = GLFW_KEY_LEFT_BRACKET;       /* [ */
    const Shortcut KEY_SPECIAL_8         = GLFW_KEY_BACKSLASH;          /* \ */
    const Shortcut KEY_SPECIAL_9         = GLFW_KEY_RIGHT_BRACKET;      /* ] */
    const Shortcut KEY_SPECIAL_10        = GLFW_KEY_GRAVE_ACCENT;       /* ` */
    const Shortcut KEY_WORLD_1           = GLFW_KEY_WORLD_1;            /* non-US #1 */
    const Shortcut KEY_WORLD_2           = GLFW_KEY_WORLD_2;            /* non-US #2 */

    /* ===== Function keys ===== */

    const Shortcut KEY_ESCAPE            = GLFW_KEY_ESCAPE;
    const Shortcut KEY_ENTER             = GLFW_KEY_ENTER;
    const Shortcut KEY_TAB               = GLFW_KEY_TAB;
    const Shortcut KEY_BACKSPACE         = GLFW_KEY_BACKSPACE;
    const Shortcut KEY_INSERT            = GLFW_KEY_INSERT;
    const Shortcut KEY_DELETE            = GLFW_KEY_DELETE;
    const Shortcut KEY_RIGHT             = GLFW_KEY_RIGHT;
    const Shortcut KEY_LEFT              = GLFW_KEY_LEFT;
    const Shortcut KEY_DOWN              = GLFW_KEY_DOWN;
    const Shortcut KEY_UP                = GLFW_KEY_UP;
    const Shortcut KEY_PAGE_UP           = GLFW_KEY_PAGE_UP;
    const Shortcut KEY_PAGE_DOWN         = GLFW_KEY_PAGE_DOWN;
    const Shortcut KEY_HOME              = GLFW_KEY_HOME;
    const Shortcut KEY_END               = GLFW_KEY_END;
    const Shortcut KEY_CAPS_LOCK         = GLFW_KEY_CAPS_LOCK;
    const Shortcut KEY_SCROLL_LOCK       = GLFW_KEY_SCROLL_LOCK;
    const Shortcut KEY_NUM_LOCK          = GLFW_KEY_NUM_LOCK;
    const Shortcut KEY_PRINT_SCREEN      = GLFW_KEY_PRINT_SCREEN;
    const Shortcut KEY_PAUSE             = GLFW_KEY_PAUSE;
    const Shortcut KEY_F1                = GLFW_KEY_F1;
    const Shortcut KEY_F2                = GLFW_KEY_F2;
    const Shortcut KEY_F3                = GLFW_KEY_F3;
    const Shortcut KEY_F4                = GLFW_KEY_F4;
    const Shortcut KEY_F5                = GLFW_KEY_F5;
    const Shortcut KEY_F6                = GLFW_KEY_F6;
    const Shortcut KEY_F7                = GLFW_KEY_F7;
    const Shortcut KEY_F8                = GLFW_KEY_F8;
    const Shortcut KEY_F9                = GLFW_KEY_F9;
    const Shortcut KEY_F10               = GLFW_KEY_F10;
    const Shortcut KEY_F11               = GLFW_KEY_F11;
    const Shortcut KEY_F12               = GLFW_KEY_F12;
    const Shortcut KEY_F13               = GLFW_KEY_F13;
    const Shortcut KEY_F14               = GLFW_KEY_F14;
    const Shortcut KEY_F15               = GLFW_KEY_F15;
    const Shortcut KEY_F16               = GLFW_KEY_F16;
    const Shortcut KEY_F17               = GLFW_KEY_F17;
    const Shortcut KEY_F18               = GLFW_KEY_F18;
    const Shortcut KEY_F19               = GLFW_KEY_F19;
    const Shortcut KEY_F20               = GLFW_KEY_F20;
    const Shortcut KEY_F21               = GLFW_KEY_F21;
    const Shortcut KEY_F22               = GLFW_KEY_F22;
    const Shortcut KEY_F23               = GLFW_KEY_F23;
    const Shortcut KEY_F24               = GLFW_KEY_F24;
    const Shortcut KEY_F25               = GLFW_KEY_F25;
    const Shortcut KEY_KP_0              = GLFW_KEY_KP_0;
    const Shortcut KEY_KP_1              = GLFW_KEY_KP_1;
    const Shortcut KEY_KP_2              = GLFW_KEY_KP_2;
    const Shortcut KEY_KP_3              = GLFW_KEY_KP_3;
    const Shortcut KEY_KP_4              = GLFW_KEY_KP_4;
    const Shortcut KEY_KP_5              = GLFW_KEY_KP_5;
    const Shortcut KEY_KP_6              = GLFW_KEY_KP_6;
    const Shortcut KEY_KP_7              = GLFW_KEY_KP_7;
    const Shortcut KEY_KP_8              = GLFW_KEY_KP_8;
    const Shortcut KEY_KP_9              = GLFW_KEY_KP_9;
    const Shortcut KEY_KP_DECIMAL        = GLFW_KEY_KP_DECIMAL;
    const Shortcut KEY_KP_DIVIDE         = GLFW_KEY_KP_DIVIDE;
    const Shortcut KEY_KP_MULTIPLY       = GLFW_KEY_KP_MULTIPLY;
    const Shortcut KEY_KP_SUBTRACT       = GLFW_KEY_KP_SUBTRACT;
    const Shortcut KEY_KP_ADD            = GLFW_KEY_KP_ADD;
    const Shortcut KEY_KP_ENTER          = GLFW_KEY_KP_ENTER;
    const Shortcut KEY_KP_EQUAL          = GLFW_KEY_KP_EQUAL;
    const Shortcut KEY_LEFT_SHIFT        = GLFW_KEY_LEFT_SHIFT;
    const Shortcut KEY_LEFT_CONTROL      = GLFW_KEY_LEFT_CONTROL;
    const Shortcut KEY_LEFT_ALT          = GLFW_KEY_LEFT_ALT;
    const Shortcut KEY_LEFT_SUPER        = GLFW_KEY_LEFT_SUPER;
    const Shortcut KEY_RIGHT_SHIFT       = GLFW_KEY_RIGHT_SHIFT;
    const Shortcut KEY_RIGHT_CONTROL     = GLFW_KEY_RIGHT_CONTROL;
    const Shortcut KEY_RIGHT_ALT         = GLFW_KEY_RIGHT_ALT;
    const Shortcut KEY_RIGHT_SUPER       = GLFW_KEY_RIGHT_SUPER;
    const Shortcut KEY_MENU              = GLFW_KEY_MENU;

    /* ===== Modifiers ===== */

    //These are just the GLFW versions, shifted by MOD_BITSHIFT, so they can be combined nicely with the scancodes in one number.
    const Shortcut MOD_BITSHIFT          = 24;
    const Shortcut MOD_SHIFT             = GLFW_MOD_SHIFT      << MOD_BITSHIFT;
    const Shortcut MOD_CONTROL           = GLFW_MOD_CONTROL    << MOD_BITSHIFT;
    const Shortcut MOD_ALT               = GLFW_MOD_ALT        << MOD_BITSHIFT;
    const Shortcut MOD_SUPER             = GLFW_MOD_SUPER      << MOD_BITSHIFT;
    const Shortcut MOD_CAPS_LOCK         = GLFW_MOD_CAPS_LOCK  << MOD_BITSHIFT;
    const Shortcut MOD_NUM_LOCK          = GLFW_MOD_NUM_LOCK   << MOD_BITSHIFT;

    /* ===== BUTTONS ===== */

    const Shortcut MOUSE_DEVICE = (1 << 30);

    const Shortcut MOUSE_BUTTON_1 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_1;
    const Shortcut MOUSE_BUTTON_2 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_2;
    const Shortcut MOUSE_BUTTON_3 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_3;
    const Shortcut MOUSE_BUTTON_4 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_4;
    const Shortcut MOUSE_BUTTON_5 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_5;
    const Shortcut MOUSE_BUTTON_6 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_6;
    const Shortcut MOUSE_BUTTON_7 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_7;
    const Shortcut MOUSE_BUTTON_8 = MOUSE_DEVICE + GLFW_MOUSE_BUTTON_8;
    const Shortcut MOUSE_BUTTON_LAST   = GLFW_MOUSE_BUTTON_LAST;
    const Shortcut MOUSE_BUTTON_LEFT   = GLFW_MOUSE_BUTTON_LEFT;
    const Shortcut MOUSE_BUTTON_RIGHT  = GLFW_MOUSE_BUTTON_RIGHT;
    const Shortcut MOUSE_BUTTON_MIDDLE = GLFW_MOUSE_BUTTON_MIDDLE;

    /* ===== Gamepad ===== */

    const Shortcut GAMEPAD_DEVICE = (2 << 30);

    const Shortcut  JOYSTICK_1  = GAMEPAD_DEVICE + GLFW_JOYSTICK_1;
    const Shortcut  JOYSTICK_2  = GAMEPAD_DEVICE + GLFW_JOYSTICK_2;
    const Shortcut  JOYSTICK_3  = GAMEPAD_DEVICE + GLFW_JOYSTICK_3;
    const Shortcut  JOYSTICK_4  = GAMEPAD_DEVICE + GLFW_JOYSTICK_4;
    const Shortcut  JOYSTICK_5  = GAMEPAD_DEVICE + GLFW_JOYSTICK_5;
    const Shortcut  JOYSTICK_6  = GAMEPAD_DEVICE + GLFW_JOYSTICK_6;
    const Shortcut  JOYSTICK_7  = GAMEPAD_DEVICE + GLFW_JOYSTICK_7;
    const Shortcut  JOYSTICK_8  = GAMEPAD_DEVICE + GLFW_JOYSTICK_8;
    const Shortcut  JOYSTICK_9  = GAMEPAD_DEVICE + GLFW_JOYSTICK_9;
    const Shortcut JOYSTICK_10  = GAMEPAD_DEVICE + GLFW_JOYSTICK_10;
    const Shortcut JOYSTICK_11  = GAMEPAD_DEVICE + GLFW_JOYSTICK_11;
    const Shortcut JOYSTICK_12  = GAMEPAD_DEVICE + GLFW_JOYSTICK_12;
    const Shortcut JOYSTICK_13  = GAMEPAD_DEVICE + GLFW_JOYSTICK_13;
    const Shortcut JOYSTICK_14  = GAMEPAD_DEVICE + GLFW_JOYSTICK_14;
    const Shortcut JOYSTICK_15  = GAMEPAD_DEVICE + GLFW_JOYSTICK_15;
    const Shortcut JOYSTICK_16  = GAMEPAD_DEVICE + GLFW_JOYSTICK_16;

    const Shortcut GAMEPAD_BUTTON_A            = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_A;
    const Shortcut GAMEPAD_BUTTON_B            = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_B;
    const Shortcut GAMEPAD_BUTTON_X            = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_X;
    const Shortcut GAMEPAD_BUTTON_Y            = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_Y;
    const Shortcut GAMEPAD_BUTTON_LEFT_BUMPER  = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_LEFT_BUMPER;
    const Shortcut GAMEPAD_BUTTON_RIGHT_BUMPER = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER;
    const Shortcut GAMEPAD_BUTTON_BACK         = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_BACK;
    const Shortcut GAMEPAD_BUTTON_START        = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_START;
    const Shortcut GAMEPAD_BUTTON_GUIDE        = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_GUIDE;
    const Shortcut GAMEPAD_BUTTON_LEFT_THUMB   = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_LEFT_THUMB;
    const Shortcut GAMEPAD_BUTTON_RIGHT_THUMB  = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_RIGHT_THUMB;
    const Shortcut GAMEPAD_BUTTON_DPAD_UP      = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_DPAD_UP;
    const Shortcut GAMEPAD_BUTTON_DPAD_RIGHT   = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_DPAD_RIGHT;
    const Shortcut GAMEPAD_BUTTON_DPAD_DOWN    = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_DPAD_DOWN;
    const Shortcut GAMEPAD_BUTTON_DPAD_LEFT    = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_DPAD_LEFT;

    const Shortcut GAMEPAD_BUTTON_CROSS        = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_A;
    const Shortcut GAMEPAD_BUTTON_CIRCLE       = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_B;
    const Shortcut GAMEPAD_BUTTON_SQUARE       = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_X;
    const Shortcut GAMEPAD_BUTTON_TRIANGLE     = GAMEPAD_DEVICE + GLFW_GAMEPAD_BUTTON_Y;

    const Shortcut GAMEPAD_AXIS_LEFT_X         = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_LEFT_X;
    const Shortcut GAMEPAD_AXIS_LEFT_Y         = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_LEFT_Y;
    const Shortcut GAMEPAD_AXIS_RIGHT_X        = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_RIGHT_X;
    const Shortcut GAMEPAD_AXIS_RIGHT_Y        = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_RIGHT_Y;
    const Shortcut GAMEPAD_AXIS_LEFT_TRIGGER   = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_LEFT_TRIGGER;
    const Shortcut GAMEPAD_AXIS_RIGHT_TRIGGER  = GAMEPAD_DEVICE + GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER;

    #pragma endregion

    typedef struct Area {
        Shortcut m_Shortcut = 0;
        uint32_t m_PosZ = 0;
        virtual bool containsPoint(double xpos, double ypos);
        bool b_IsHovered = false;
        bool mb_ConsumeInput = true;
        void(*m_EnterCallback)(void) = nullptr;
        void(*m_InputCallback)(void) = nullptr;
        void(*m_LeaveCallback)(void) = nullptr;

        Area() {};
        virtual ~Area();
    } Area;

#if 0 /* ===== Below are examples for the containesPoint method for some simple shapes: ===== */

    typedef struct Rectangle : public Area {
        uint32_t m_PosX = 0, m_PosY = 0, m_Width = 0, m_Height = 0;
        bool containsPoint(double xPos, double yPos) {
            return (xPos >= m_PosX && xPos < m_PosX + m_Width && yPos >= m_PosY && yPos < m_PosY + m_Height);
        };
    } Rectangle;    

    typedef struct Circle : public Area {
        uint32_t m_XPos = 0, m_YPos = 0, m_Radius = 0;
        bool containsPoint(double xPos, double yPos) {
            double dx = xPos - m_XPos, dy = yPos - m_YPos;
            return (std::sqrt(dx * dx + dy * dy) < m_Radius);
        };
    } Circle;

    typedef struct Triangle : public Area { //TODO
    } Triangle;

#endif


    typedef void(*CursorMoveCallback)(double, double);
    typedef void(*ShortcutCallback)(int, Shortcut);
    typedef unsigned int UnicodeCodePoint;
    typedef void(*CharacterCallback)(UnicodeCodePoint);

    void registerCursorMoveCallback(GLFWwindow* p_Window, const CursorMoveCallback callback);

    void eraseCursorMoveCallback(GLFWwindow* p_Window, const CursorMoveCallback callback);

    void eraseAllCursorMoveCallbacks(GLFWwindow* p_Window);

    void registerInputCallback(GLFWwindow* p_Window, Shortcut shortcut, ShortcutCallback callback);

    void eraseInputCallback(GLFWwindow* p_Window, Shortcut shortcut, ShortcutCallback callback);

    void eraseAllInputCallbacks(GLFWwindow* p_Window, ShortcutCallback callback);

    void buttonCallback(GLFWwindow* p_Window, int button, int action, int mods);

    /**
     * @param p_Window
     * @param key is a token for each keyboard key. Some rare or custom keyboardkey may have no tokens, then this is GLFW_KEY_UNKNOWN.
     * @param scancode is a unique idetifier for each key, that is platform-specific, but persistant over time.
     * @param action is one of GLFW_PRESS, GLFW_REPEAT, od GLFW_RELEASE.
     */
    void keyCallback(GLFWwindow* p_Window, int key, int scancode, int action, int mods);

    void registerCharacterCallback(GLFWwindow* p_Window, const CharacterCallback callback);

    void eraseCharacterCallback(GLFWwindow* p_Window, const CharacterCallback callback);

    void eraseAllCharacterCallbacks(GLFWwindow* p_Window);

    void initWindowInput(GLFWwindow* p_Window);

    void init();

    void shutdown();

} // namespace creato::input