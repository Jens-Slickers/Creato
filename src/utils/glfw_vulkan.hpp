/**
 * 
 */

#pragma once

//Include GLFW and Vulkan (Vulkan is included indirectly through GLFW)
#define GLFW_DLL
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define CREATO_APPLICATION_NAME "Creato"
#define CREATO_APPLICATION_VERSION VK_MAKE_VERSION(0, 0, 0)