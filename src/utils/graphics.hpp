/**
 * 
 */

#pragma once

//STD
#include <vector>
#include <array>
#include <string>
#include <unordered_set>

//GLFW and Vulkan
#include <utils/glfw_vulkan.hpp>

//GLM
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/hash.hpp>

namespace creato::graphics {

    typedef enum CreatoGraphicsResult {
        LGR_SUCCESS = 0,
        LGR_ERROR = 1,
        LGR_CREATE_ERROR,
        LGR_ALLOC_ERROR,
        LGR_BIND_ERROR,
        LGR_BEGIN_ERROR,
        LGR_THROW_ERROR
    } CreatoGraphicsResult;

    typedef struct CreatoGraphicsResultInfo {
        CreatoGraphicsResult lgResult = LGR_SUCCESS;
        VkResult vkResult = VK_SUCCESS;
        const char* message;
    } CreatoGraphicsResultInfo;

    typedef struct Vertex {

        glm::vec3 pos;
        glm::vec2 texCoord;

        Vertex() {}
        Vertex(glm::vec3 pos, glm::vec2 texCoord) : pos(pos), texCoord(texCoord) {}

        bool operator==(const Vertex& other) const {
            return pos == other.pos && texCoord == other.texCoord;
        }

        static std::array<VkVertexInputAttributeDescription, 2> getAttributeDescriptions() {
            std::array<VkVertexInputAttributeDescription, 2> vertexInputAttributeDescriptions; //Descr for each shader input
            vertexInputAttributeDescriptions[0].location = 0;
            vertexInputAttributeDescriptions[0].binding = 0;                        //synced with binding description
            vertexInputAttributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;       //vec2
            vertexInputAttributeDescriptions[0].offset = offsetof(Vertex, pos);
            vertexInputAttributeDescriptions[1].location = 1;
            vertexInputAttributeDescriptions[1].binding = 0;
            vertexInputAttributeDescriptions[1].format = VK_FORMAT_R32G32_SFLOAT;
            vertexInputAttributeDescriptions[1].offset = offsetof(Vertex, texCoord);
            return vertexInputAttributeDescriptions;
        }

        static VkVertexInputBindingDescription getBindingDescription() {
            VkVertexInputBindingDescription vertexInputBindingDescription;
            vertexInputBindingDescription.binding = 0;
            vertexInputBindingDescription.stride = sizeof(Vertex);
            vertexInputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return vertexInputBindingDescription;
        }

    } Vertex;

    typedef struct UniformBufferObject {
        alignas(16) glm::mat4 model;
        alignas(16) glm::mat4 viewProjection;
    } UniformBufferObject;

    typedef struct Frame {

        VkCommandBuffer commandBuffer;
        VkImageView imageView;
        VkFramebuffer frameBuffer; 
        VkFence frameFence = VK_NULL_HANDLE;
    } Frame;

    typedef struct FrameSyncSet {
        VkSemaphore imageAquiredSemaphore;
        VkSemaphore renderingCompleteSemaphore;
        VkFence inFlightFence;
    } FrameSyncSet;

    typedef struct Buffer {
        VkBuffer buffer;
        VkDeviceMemory memory;
        VkDeviceSize size;
    } Buffer;

    using RGBA8 = std::array<unsigned char, 4>; //TODO shbortcut where alpha always 255
    using RGBA8_M = std::vector<unsigned char>; //TODO shbortcut where alpha always 255

    struct Model;

    class Node;

    typedef struct Window {

        GLFWwindow* glfwWindow;
        VkExtent2D windowSize = {900, 600};

        VkSurfaceKHR surface;
        VkSurfaceFormatKHR surfaceFormat;

        VkSwapchainKHR swapchain = VK_NULL_HANDLE;
        uint32_t frameIndex;
        std::vector<Frame> frames;
        uint32_t syncIndex = 0;
        std::vector<FrameSyncSet> syncSets;

        VkImage depthImage;
        VkDeviceMemory depthImageMemory;
        VkImageView depthImageView;

        VkImage colorImage;
        VkDeviceMemory colorImageMemory;
        VkImageView colorImageView;

        //TODO: make a system that is easily expandable with more shaders etc.
        std::unordered_set<Model*> sceneRenderList;
        std::unordered_set<Model*> transparentInterfaceRenderList;
        std::unordered_set<Model*> opaqueInterfaceRenderList;
        bool b_ReRecordCommandBuffers;

        Node* root = nullptr;
        
    } Window;

    class Node {
        public:

            Node() {};

            virtual ~Node();

            void destroy(bool recursive = true);

            void activate(Window* p_Window, bool recursive = true);

            void deactivate(bool recursive = true);

            virtual void onActivate(Window* /*p_Window*/) {};
            virtual void onDeactivate() {};

            void show(bool recursive = true);

            void hide(bool recursive = true);

            virtual void onShow() {};
            virtual void onHide() {};

            // Use this function to insert nodes in a scene tree.
            // DO NOT USE IT TO CHANGE A NODE'S POSITION IN THE SCENE TREE, USE changeParent FOR THAT.
            // Calls node->onAttachedToParent(), and onAttachChild(node) in this order.
            void attachChild(Node* node);

            // Does not delete the given node
            // Calls node->onDetachedFromParent(nullptr), and onDetachChild(node) in this order.
            void detachChild(Node* node);

            virtual void onAttachChild(Node* /*attachedChild*/) {};
            virtual void onDetachChild(Node* /*detachedChild*/) {};

            // Only use this when changing a node from one parent to another. Any nullptr will throw!
            // Calls parent->detachChild(this), and newParent->attachChild(this) in this order.
            void changeParent(Node* newParent);

            //This may be called with nullptr!
            virtual void onAttachedToParent() {};
            virtual void onDetachedFromParent(Node* /*oldParent*/) {};

            void detachFromParent();

        protected:

            std::unordered_set<Node*> children = std::unordered_set<Node*>(0);
            Node* parent = nullptr;

            bool visible = true;
    };

    Node* createNode();

    void destroyNode(Node* node, bool recursive = true);

    struct Texture;

    Texture* createTexture(std::string texturePath, bool b_IsMultiModelTexture = true);
    Texture* createSingleColorTexture(const RGBA8& hexColor, bool b_IsMultiModelTexture = true);
    Texture* createMultiColorTexture(const RGBA8_M& hexColor, bool b_IsMultiModelTexture);
    std::vector<Vertex> getCharacterVertices(std::string fontFileName, unsigned long codepoint, glm::vec2& pos, graphics::Texture** p_Texture);
    void createFontBitmaps(const std::vector<std::pair<std::string, uint32_t>>& fonts);
    void createFontSDFs(const std::vector<std::string>& fonts);
    void destroyTexture(Texture* p_Texture);

    typedef struct Texture {

        bool b_IsMultiModelTexture;

        glm::ivec2 size;

        VkImage image;
        VkDeviceMemory imageMemory;
        VkImageView imageView;
        VkImageLayout imageLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
        VkSampler sampler;

        uint32_t mipLevels;

        Texture() {}
        ~Texture() { //This allows to just delete a texture instead of calling destroy texture manually.
            destroyTexture(this);
        }

    } Texture;

    struct Mesh;
 
    Mesh* createMesh(std::string meshPath, bool b_IsMultiModelMesh = true);
    void destroyMesh(Mesh* p_Mesh);

    typedef struct Mesh {

        bool b_IsMultiModelMesh;

        std::vector<Vertex> v_VertexList;
        Buffer vertexBufferCase;
        std::vector<uint32_t> v_IndexList; // supports 2^32 vertecies!
        Buffer indexBufferCase;

        Mesh() {}
        ~Mesh() {
            destroyMesh(this);
        }

    } Mesh;

    Model* createModel(Mesh* p_Mesh, Texture* p_Texture, Window* p_Window);
    Model* createModel(std::string path, Texture* p_Texture, Window* p_Window);
    Model* createModel(std::string meshPath, std::string texturePath, Window* p_Window);
    Model* createInterfaceModel(Mesh* p_Mesh, Texture* p_Texture, Window* p_Window);
    Model* createInterfaceModel(float posX, float posY, float posZ, Texture* p_Texture, Window* p_Window, bool opaque = false);
    void destroyModel(Model* p_Model);

    typedef struct Model {
        Buffer uniformBufferCase;
        VkDescriptorPool descriptorPool;
        VkDescriptorSet descriptorSet;

        Texture* p_Texture;
        Mesh* p_Mesh;

        //p_Window ptr must be valid throughout the entire lifetime of p_Model.
        Window* p_Window;

        glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
        glm::quat rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f); //wxyz
        glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);

        void (*updateMPVFunc)(Model*); //TODO

        Model() {}
        ~Model() {
            destroyModel(this);
        }

    } Model;

    /* ========== Functions ========== */

    void init();

    Window* createWindow(const char* title, Node* root = nullptr);
    void changeWindowRoot(Window* p_Window, Node* newRoot);
    void eraseModelFromRenderLists(Model* p_Model);
    void insertModelIntoRenderLists(Model* p_Model);
    void destroyWindow(Window* window);

    CreatoGraphicsResultInfo* drawframe(Window* window);

    void shutdown();

    void createVertexBuffer(Buffer& vertexBufferCase, std::vector<Vertex>& verticies); //TODO static
    void createIndexBuffer(Buffer &uniformBufferCase, std::vector<uint32_t> &indices); //TODO static

    void updateModel(Model* model);

} // namespace creato::graphics

namespace std {
    template<> struct hash<creato::graphics::Vertex> {
        size_t operator()(creato::graphics::Vertex const& vertex) const {
            return ((hash<glm::vec3>()(vertex.pos) ^ (hash<glm::vec2>()(vertex.texCoord) << 1)) >> 1);
        }
    };
} // namespace std